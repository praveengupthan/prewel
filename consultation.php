<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs</title>  
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Consultation</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page">Consultation</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body-->
    <div class="subpage-body"> 
        <!-- section -->
        <div class="whitebox py-5">
            <!-- container -->
            <div class="container">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-lg-8 text-center aos-item" data-aos="fade-up">
                    <h3 class="text-center">Consultation Services in Bengaluru</h3>
                    <p class="text-center">Prewel Labs doesn’t just provide the testing support but it also provides end to end consultation so that our customers get everything they are looking for under one roof</p>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->          
            <!-- row -->
            <div class="row justify-content-center pt-2 pt-sm-4">
                <!-- col -->
                <div class="col-6 col-md-4 col-lg-2  icon-div aos-item" data-aos="fade-down">                               
                    <div class="icon">
                        <span class="icon-tools-and-utensils icomoon"></span>
                    </div>  
                    <p> Food safety implementation </p>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-6 col-md-4 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                    <div class="icon">
                        <span class="icon-science icomoon"></span>
                    </div>  
                    <p> Food testing lab set up (Chemical, Analytical and Microbiology)  </p>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-6 col-md-4 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                    <div class="icon">
                        <span class="icon-interface-2 icomoon"></span>
                    </div>  
                    <p> Development  and standardization of lab documentation  </p>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-6 col-md-4 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                    <div class="icon">
                        <span class="icon-bar icomoon"></span>
                    </div>  
                    <p> Food product design and development  </p>
                </div>
                <!--/ col --> 
                <!-- col -->
                <div class="col-6 col-md-4 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                    <div class="icon">
                        <span class="icon-tools-and-utensils-2 icomoon"></span>
                    </div>  
                    <p> Restaurant & catering food safety consulting </p>
                </div>
                <!--/ col -->                          
            </div>
            <!--/ row -->  
            <!-- row -->
            <div class="row justify-content-center border-top pt-5">
                <!-- col -->
                <div class="col-lg-12 text-center aos-item" data-aos="fade-up">                   
                    <p class="text-center">As a part of the consultation programme, Prewel Labs also guides and assists a lot of our B2B and B2C clients with it’s Audit consultation. These audits help our clients in keeping them aware and prepared with the latest regulatory and compliance requirements that will further increase their growth opportunities.</p>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->   
            <!-- row -->
            <div class="row justify-content-center pt-2 pt-sm-4">
                <!-- col -->
                <div class="col-6 col-md-4 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                    <div class="icon">
                        <span class="icon-tools-and-utensils icomoon"></span>
                    </div>  
                    <p> Food safety Audits </p>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-6 col-md-4 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                    <div class="icon">
                        <span class="icon-interface-2 icomoon"></span>
                    </div>  
                    <p> Food safety documentation  </p>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-6 col-md-4 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                    <div class="icon">
                        <span class="icon-search icomoon"></span>
                    </div>  
                    <p> Food inspection services  </p>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-6 col-md-4 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                    <div class="icon">
                        <span class="icon-Page-1 icomoon"></span>
                    </div>  
                    <p> 3rd party Quality audits as per FSSAI, IFS and FSMS etc. </p>
                </div>
                <!--/ col --> 
                <!-- col -->
                <div class="col-6 col-md-4 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                    <div class="icon">
                        <span class="icon-business-and-finance icomoon"></span>
                    </div>  
                    <p> Food analysis and regulatory guidance  </p>
                </div>
                <!--/ col --> 
                <!-- col -->
                <div class="col-6 col-md-4 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                    <div class="icon">
                        <span class="icon-direction icomoon"></span>
                    </div>  
                    <p> Guidance on NABL and FSSAI accreditation </p>
                </div>
                <!--/ col -->                                 
            </div>
            <!--/ row -->         
        </div>
        <!--/ container -->
    </div>
    <!--/ sectioin -->

         <!--  whitebox -->        
         <div class="whitebox py-3">
            <!-- container-->
            <div class="container">
                <!-- row -->
                <div class="row"> 
                     <!-- col -->
                     <div class="col-lg-6 align-self-center aos-item" data-aos="fade-up">
                       <h2>Training & Support</h2>
                       <p>Training has always been a critical part of any industry’s success story. This allows your entire organization to be better equipped to tackle the tough competition. Our certified instructors can provide your organization with customized training course designed exclusively for you based on the gaps identified. Our instructors use slides, videos and demonstrations to augment lectures and classroom discussions. Hands-on task-oriented sanitation training can also be provided for your food service employees. </p>
                    </div>
                    <!--col-->
                     <!-- col -->
                     <div class="col-lg-6 align-self-center">
                        <!-- row -->
                        <div class="row justify-content-center">
                            <!-- col -->
                            <div class="col-6 col-sm-6 icon-div aos-item" data-aos="fade-up">                               
                                <div class="icon">
                                    <span class="icon-men-1 icomoon"></span>
                                </div>  
                                <p>Management & employee training on food safety </p>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-6 col-sm-6 icon-div aos-item" data-aos="fade-down">                               
                                <div class="icon">
                                    <span class="icon-tools-and-utensils icomoon"></span>
                                </div>  
                                <p>Food quality and hygiene training </p>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                             <div class="col-6 col-sm-6 icon-div aos-item" data-aos="fade-up">                               
                                <div class="icon">
                                    <span class="icon-medal icomoon"></span>
                                </div>  
                                <p>ISO 17025, ISO 22000  In- house trainings </p>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                             <div class="col-6 col-sm-6 icon-div aos-item" data-aos="fade-down">                               
                                <div class="icon">
                                    <span class="icon-work icomoon"></span>
                                </div>  
                                <p>Guidance on NABL and FSSAI accreditation </p>
                            </div>
                            <!--/ col -->                            
                        </div>
                        <!--/ row -->                        
                    </div>
                    <!--col-->           
                </div>
                <!--/ row -->
            </div>
            <!-- container -->               
        </div>
        <!-- / Whitebox -->      
      
    </div>
    <!-- sub page body -->
    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
