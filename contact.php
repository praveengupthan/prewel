<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs Contact Us Page</title>  
    <meta name="description" content="You can call us at 08026589777 or 08886241818 for any kind of queries. You can also email us at support@prewellabs.com. We provide testing, consultation and solutions.">
    <?php include 'styles.php'?>    
    
</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Contact Us</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page">Contact</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->
    <!-- sub page body-->
    <div class="subpage-body">

    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row py-5">
            <!-- col -->
            <div class="col-lg-3 col-sm-6 col-md-6 contact-col text-center aos-item" >
                <div class="icon-col">
                    <a href="https://g.page/Prewel?share" target="_blank"><span class="icon-pin icomoon"></span></a>
                </div>
                <p class="text-center">No. 24, 22nd Main, Marenahalli, J P Nagar 2nd Phase,  Bengaluru</p>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-3 col-sm-6 col-md-6 contact-col text-center aos-item" >
                <div class="icon-col">
                   <a href="tel:+91 6366942390"> <span class="icon-phone icomoon"></span></a>
                </div>
                <p class="text-center mb-0">+91 6366942390</p>
                <!-- <p class="text-center">+91 8431414707</p> -->
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-3 col-sm-6 col-md-6 contact-col text-center aos-item">
                <div class="icon-col">
                    <span class="icon-globe icomoon"></span>
                </div>
                <p class="text-center mb-0">
                    <a href="mailto:support@prewellabs.com">support@prewellabs.com</a>
                </p>
                <p class="text-center">www.prewellabs.com</p>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-3 col-sm-6 col-md-6 contact-col text-center aos-item">
                <div class="icon-col">
                    <span class="icon-clock icomoon"></span>
                </div>
                <p class="text-center mb-0"> Monday - Friday 9 AM - 7PM </p>
                <p class="text-center">Saturday  9 AM - 5PM</p>
            </div>
            <!--/ col -->            
        </div>
        <!--/ row -->

     


        <div class="row justify-content-center">
            <!-- col -->
            <div class="col-lg-8">

            <?php     

if(isset($_POST['submitContact'])){
$to = "support@prewellabs.com"; 
$subject = "Mail From ".$_POST['name'];
$message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<p>".$_POST['name']." has sent mail!</p>
<table>
<tr>
<th align='left'>Name</th>
<td>".$_POST['name']."</td>
</tr>
<tr>
<th align='left'>Contact Number</th>
<td>".$_POST['phone']."</td>
</tr>
<tr>
<th align='left'>Email</th>
<td>".$_POST['email']."</td>
</tr>
<tr>
<th align='left'>Subject</th>
<td>".$_POST['sub']."</td>
</tr>
<tr>
<th align='left'>Message</th>
<td>".$_POST['msg']."</td>
</tr>
</table>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From:' .$_POST['name']. "\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

mail($to,$subject,$message,$headers);   

//success mesage
?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
  Mail Sent Successfully. Thank you <?= $_POST['name'] ?>, we will contact you shortly.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<?php

// echo "<p style='color:green'>Mail Sent. Thank you " . $_POST['name'] . ", we will contact you shortly.</p>";
}
?>

             <!-- form -->
            <form id="contact_form" class="form border-top py-md-5" action="" method="post">
                <!-- row -->
                <div class="row">
                    <!-- col --->
                    <div class="col-lg-12 aos-item" >
                        <h4 class="pt-4 pt-sm-2">Get in touch with us for a Quick Call</h4>
                        <p>Send the following details, our Team will contact you shortly</p>
                    
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 col-md-6 aos-item" >
                        <div class="form-group">
                            <label>Name<span class="mandatory">*</span></label>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Write your Name" name="name" required>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->     
                    
                    <!-- col -->
                    <div class="col-lg-6 col-md-6 aos-item">
                        <div class="form-group">
                            <label>Email <span class="mandatory">*</span></label>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Write your Email" name="email" required>
                            </div>
                        </div>
                    </div>
                    <!--/ col --> 

                    <!-- col -->
                    <div class="col-lg-6 col-md-6 aos-item">
                        <div class="form-group">
                            <label>Contact Number <span class="mandatory">*</span></label>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Write your Contact Number" name="phone" required>
                            </div>
                        </div>
                    </div>
                    <!--/ col --> 

                    <!-- col -->
                    <div class="col-lg-6 col-md-6 aos-item">
                        <div class="form-group">
                            <label>Subject <span class="mandatory">*</span></label>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Write Subject" name="sub" required>
                            </div>
                        </div>
                    </div>
                    <!--/ col --> 

                    <!-- col -->
                    <div class="col-lg-12 aos-item">
                        <div class="form-group">
                            <label>Message</label>
                            <div class="input-group">
                            <textarea style="height:120px;" class="form-control" placeholder="Write Message" name="msg"></textarea>
                            </div>
                        </div>
                    </div>
                    <!--/ col --> 

                    <!-- col -->
                    <div class="col-lg-12 aos-item">
                        <input type="submit" class="btn btn-primary g-recaptcha" value="Submit" name="submitContact">                  
                    </div>
                    <!--/ col --> 

                </div>
                <!-- row -->
            </form>
            <!--/ form -->

            </div>
            <!--/ col -->
        </div>

        

        <!-- map -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="map-contact mb-md-4 aos-item">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.897555917522!2d77.58490041531772!3d12.914305390893583!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae150fe43be2a1%3A0x28072feff270aec2!2sIncepBio+Private+Limited!5e0!3m2!1sen!2sin!4v1542589553402" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <!--/ map -->
    </div>
    <!--/ container -->
    
    </div>
    <!-- sub page body -->    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
