<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs NABL Accredited</title>  
    <meta name="description" content="Prewel Labs is a Trusted NABL Accredited Laboratory at Bangalore. We offer quality testing services of Water, Food, Indoor Air and Medical devices.">
    <?php include 'styles.php'?>    
 </script>

</head>
<body>
    
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <main> 
    <!--  slider -->
    <div class="slider-wrapper home-slider whitebox">
        <!-- Swiper -->
        <div class="swiper-container main-swiper">
            <div class="swiper-wrapper">
                <!-- slide 01-->
                <div class="swiper-slide">
                    <!-- cust container -->
                    <div class="cust-container">
                        <!--/ row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-6 col-sm-6 align-self-center text-left aos-item" data-aos="fade-up">
                                <div class="slide-content">                                       
                                    <h1 class="bas-text">Testing Services</h1>                            
                                    <div class="slide-txt">
                                        <p>NABL Accredited Lab </p>     
                                        <a href="tel:6366942390" class="link-btn">
                                            <span>Call us at 6366942390 </span>
                                        </a>                       
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-6 col-sm-6 slider-img align-self-center aos-item" data-aos="fade-down">
                                <div class="trans-name aos-item aos-init aos-animate" data-aos="fade-right"><img src="img/slider01.svg" alt=""></div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ cust container --> 
                </div>
                <!--/ slide 01-->

                <!-- slide 02-->
                <div class="swiper-slide">
                    <!-- cust container -->
                    <div class="cust-container">
                        <!--/ row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-6 col-sm-6 align-self-center text-left">
                                <div class="slide-content">                                       
                                    <h1 class="bas-text">Consultation</h1>                            
                                    <div class="slide-txt">
                                        <p>Providing You with More than just a Call </p>     
                                        <a href="consultation.php" class="link-btn">Read More</a>                       
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-6 col-sm-6 align-self-center">
                                <div class="trans-name aos-item aos-init aos-animate" data-aos="fade-right"><img src="img/slider02.svg" alt=""></div>
                             </div>
                             <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ cust container -->
                </div>
                <!--/ slide 02-->

                <!-- slide 03-->
                <div class="swiper-slide">
                    <!-- cust container -->
                    <div class="cust-container">
                        <!--/ row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-6 col-sm-6 align-self-center text-left">
                                <div class="slide-content">                                       
                                    <h1 class="bas-text">Solutions</h1>                            
                                    <div class="slide-txt">
                                        <p>Customised Solutions for Every Problem under One Roof </p>     
                                        <a href="solutions.php" class="link-btn">Read More</a>                       
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-6 col-sm-6 align-self-center">
                                <div class="trans-name aos-item aos-init aos-animate" data-aos="fade-right"><img src="img/slider03.svg" alt=""></div>
                             </div>
                             <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ cust container -->
                </div>     
                <!--/ slide 03-->
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>           
        </div>
    </div>
    <!--/ slider -->        

    <!-- why are we -->
    <div class="whyarev whitebox">
        <!-- container fluid -->
        <div class="container">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-10 align-self-center text-center">
                    <article class="py-5 whysection">
                        <!-- <h3 class="aos-item" data-aos="fade-up">Why are we here</h3> -->
                        <h2 class="aos-item" data-aos="fade-up">Why Are <span class="flight">We Here</span> </h2>
                        <p class="aos-item" data-aos="fade-up">Keeping in tune with the healthy living wave that has engulfed the western world, today's India is also becoming increasingly health conscious. People are taking a keen interest in being mindful about what they put in their bodies. With pollution levels reaching all time high, the need to be careful about safety, quality and information about what we consume has increased significantly. Having identified the need of the hour, Prewel Labs has been working relentlessly to come up with sustainable and affordable solutions to envisage a healthier planet.</p>   

                        <a class="aos-item whyrv fblue fsbold" data-aos="fade-down" href="javascript:void(0)" data-toggle="modal" data-target="#prewel-video"> Click to Play Video <img src="img/youtube-icon.png" alt=""> </a>                    
                    </article>
                </div>
                <!--/ col -->
                
            </div>
            <!--/row -->
        </div>
        <!--/ container fluid -->
    </div>
    <!--/ why are we -->

    <!-- services -->
    <div class="home-services">
        <h2 class="aos-item text-center" data-aos="fade-up">How can Prewel Labs <span class="flight">assist You!</span> </h2>

        <!-- services list -->
        <div class="services-list">
            <!-- services col-->
            <div class="service-col aos-item" data-aos="fade-down">
               <a href="water-testing.php">
                    <span class="icon-ph icomoon"></span>
                    <h6>Water Testing</h6>
               </a>
            </div>
            <!--/ service col -->
             <!-- services col-->
             <div class="service-col aos-item" data-aos="fade-up">
               <a href="food-testing.php">
                    <span class="icon-food icomoon"></span>
                    <h6>Food Testing</h6>
               </a>
            </div>
            <!--/ service col -->
             <!-- services col-->
             <div class="service-col aos-item" data-aos="fade-up">
               <a href="air-testing.php">
                    <span class="icon-air-pollution icomoon"></span>
                    <h6>Air Testing</h6>
               </a>
            </div>
            <!--/ service col -->
             <!-- services col-->
             <div class="service-col aos-item" data-aos="fade-down">
               <a href="environmentaltesting.php">
                    <span class="icon-factory icomoon"></span>
                    <h6>Environmental Testing</h6>
               </a>
            </div>
            <!--/ service col -->

             <!-- services col-->
             <div class="service-col aos-item" data-aos="fade-up">
               <a href="medical-device-testing.php">
                    <span class="icon-smartwatch icomoon"></span>
                    <h6>Medical Device Testing</h6>
               </a>
            </div>
            <!--/ service col -->
            
            <!-- services col-->
             <div class="service-col aos-item" data-aos="fade-down">
               <a href="pharma-testing.php">
                    <span class="icon-pharmacy icomoon"></span>
                    <h6>Pharma Testing</h6>
               </a>
            </div>
            <!--/ service col -->

             <!-- services col-->
             <div class="service-col aos-item" data-aos="fade-down">
               <span class="badge badge-primary">New</span>
               <a href="sanitizer-testing-services.php">
                    <span class="icon-hand-sanitizer-2 icomoon"></span>
                    <h6>Sanitizer Testing Services</h6>
               </a>
            </div>
            <!--/ service col -->
            
        </div>
        <!--/ services list -->
    </div>
    <!--/ services -->

    <!-- why prewel labs -->
    <div class="whyprewel whitebox py-5">
        <!-- container -fluid -->
        <div class="container-fluid">
              <!-- row -->
              <div class="row pb-5">
                <!-- col -->
                <div class="col-lg-12 text-center">
                    <articler class="home-title aos-item" data-aos="fade-up">                       
                        <h3><span>Why Choose</span> Prewellabs</h3>
                    </articler>
                </div>
                <!-- col -->
            </div>
            <!--/ row -->
            <!-- row -->
            <div class="d-flex whycol-row">
                <!-- col -->
                <div class="whycol aos-item" data-aos="fade-down">
                    <article>
                        <div class="whycol-icon">
                            <span class="icon-medal icomoon"></span>
                        </div>
                        <h4>NABL Accredited</h4>                        
                    </article>
                </div>
                <!-- col -->
                 <!-- col -->
                 <div class="whycol aos-item" data-aos="fade-up">
                    <article>
                        <div class="whycol-icon">                            
                            <span class="icon-rate icomoon"></span>
                        </div>
                        <h4>Rated Highest on Google </h4>                       
                    </article>
                </div>
                <!-- col -->
                 <!-- col -->
                 <div class="whycol aos-item" data-aos="fade-down">
                    <article>
                        <div class="whycol-icon">
                            <span class="icon-XMLID_806 icomoon"></span>
                        </div>
                        <h4>Fastest turnaround time </h4>                        
                    </article>
                </div>
                <!-- col -->
                 <!-- col -->
                 <div class="whycol aos-item" data-aos="fade-up">
                    <article>
                        <div class="whycol-icon">
                            <span class="icon-Outline icomoon"></span>
                        </div>
                        <h4>Strong technical back end support</h4>                       
                    </article>
                </div>
                <!-- col -->
                  <!-- col -->
                  <div class="whycol aos-item" data-aos="fade-down">
                    <article>
                        <div class="whycol-icon">
                            <span class="icon-Group-176 icomoon"></span>
                        </div>
                        <h4>Internationally accepted test reports</h4>                       
                    </article>
                </div>
                <!-- col -->
                  <!-- col -->
                  <div class="whycol aos-item" data-aos="fade-up">
                    <article>
                        <div class="whycol-icon">
                            <span class="icon-discount icomoon"></span>
                        </div>
                        <h4>Affordable</h4>                       
                    </article>
                </div>
                <!-- col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container fluid -->
    </div>
    <!--/ why prewel labs -->

    <!-- about prewel labs -->
    <div class="about py-5">
        <span class="transtitle">About us</span>
        <!-- custom container -->
        <div class="cust-container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-6 col-sm-6 align-self-center">
                   <div class="p-md-5">
                    <articler class="home-title aos-item" data-aos="fade-up">                      
                        <h3><span>Who </span> are we</h3>
                    </articler>
                    <p class="text-justify pt-3 aos-item" data-aos="fade-down">Prewel Labs is a NABL accredited Laboratory with headquarters at Bengaluru. It is a unit of IncepBio Pvt. Ltd., a privately held company offering comprehensive services in quality testing of Water, Food, Indoor air and Medical devices. Prewel Labs also provides consultation, allowing its customers to have one point of contact for all their testing and consultation needs. Prewel Labs supports a wide range of customers that includes IT companies, Apartments, MNCs, Hospitality industry, Food Caterers, Schools, NGOs, Residential complexes, Shopping malls and Manufacturing companies.</p>
                   </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-md-6 col-sm-6 aos-item align-self-center" data-aos="fade-down">
                    <img src="img/about-diagram.svg" alt="" class="img-fluid">
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ custom container -->
    </div>
    <!--/ about prewel labs -->

    <!-- testing process -->
    <div class="testing-process">
       

        <!-- process bottom -->
        <div class="processBlocks">
             <!-- container  -->
        <div class="container">
            <!-- row -->
            <div class="row py-4">
                <!-- col -->
                <div class="col-lg-12 text-center">
                    <articler class="home-title">                       
                        <h3 class="pb-5"><span>How </span> We Work</h3>
                    </articler>
                </div>
                <!-- col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container  -->
            <!-- container--> 
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-2 col-sm-4 col-6 text-md-center aos-item" data-aos="fade-up">
                        <div class="process-col">
                            <span class="icon-lab icomoon"></span>
                        </div>
                        <p class="fblue process-title">1. Sample collection</p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-md-2 col-sm-4 col-6 text-md-center aos-item" data-aos="fade-down">                       
                        <div class="process-col">
                            <span class="icon-truck icomoon"></span>
                        </div>
                        <p class="fblue process-title">2. Transportation</p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-md-2 col-sm-4 col-6 text-md-center aos-item" data-aos="fade-up">                       
                        <div class="process-col">
                            <span class="icon-hourglass-o icomoon"></span>
                        </div>
                        <p class="fblue process-title">3. Processing Sample</p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-md-2 col-sm-4 col-6 text-md-center aos-item" data-aos="fade-down">                       
                        <div class="process-col">
                            <span class="icon-data icomoon"></span>
                        </div>
                        <p class="fblue process-title">4. Analysis</p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-md-2 col-sm-4 col-6 text-md-center aos-item" data-aos="fade-up">                       
                        <div class="process-col">
                            <span class="icon-Group-176 icomoon"></span>
                        </div>
                        <p class="fblue process-title">5. Report Generation</p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-md-2 col-sm-4 col-6 text-md-center aos-item" data-aos="fade-down">                       
                        <div class="process-col">
                            <span class="icon-Outline icomoon"></span>
                        </div>
                        <p class="fblue process-title">6. Follow up Consultation</p>
                    </div>
                    <!--/ col -->
                </div>
                <!-- /row -->
            </div>
            <!--/ container--> 
        </div>
        <!--/ process bottom -->
    </div>
    <!--/ testing process -->

    <!-- testimonials-->
    <div class="testimonials">
        <!-- container -->
        <div class="container">
            <!-- white box --> 
            <div class="whitebox py-md-4">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-10">
                        <!-- title -->
                        <articler class="home-title text-center aos-item" data-aos="fade-up">
                            <p>What do our clients say</p>
                            <h3><span>Testimonials</span></h3>
                        </articler>
                        <!--/ title -->

                        <!-- swiper-->
                        <div class="swiper-container test-in">
                            <div class="swiper-wrapper">
                            <div class="swiper-slide" data-swiper-autoplay="8000">
                                <article>
                                    <p class="text-center pb-0 mb-0"><span class="icon-quote-left icomoon h3"></span></p>
                                    <p>We are very pleased with the quality of service that your company provides. We sincerely appreciate your responsiveness and the way you conduct business. We can recommend your company to others because of our satisfaction with your service. We look forward to doing business with you for years to come.</p>
                                    <h5 class="h5 pb-0 mb-0">Thanigesan</h5>
                                    <div><small><i><img class="reviewlowo" src="img/linlogo.png"></i></small></div>
                                </article>
                              </div>
                              <div class="swiper-slide" data-swiper-autoplay="8000">
                                  <article>
                                      <p class="text-center pb-0 mb-0"><span class="icon-quote-left icomoon h3"></span></p>
                                      <p>I am very happy with IncepBio/your service. You have been prompt, reliable and I state my lab's qualifications, so that they know I am providing a very high quality product.</p>
                                      <h5 class="h5 pb-0 mb-0">Sukitha</h5>
                                      <div><small><i> <img class="reviewlowo" src="img/hblogo.png"> Bengaluru</i></small></div>
                                  </article>
                              </div>
                              <div class="swiper-slide" data-swiper-autoplay="8000">
                                <article>
                                    <p class="text-center pb-0 mb-0"><span class="icon-quote-left icomoon h3"></span></p>
                                    <p>We are very pleased with the quality of service that your company provides. We sincerely appreciate your responsiveness and the way you conduct business. We can recommend your company to others because of our satisfaction with your service. We look forward to doing business with you for years to come.</p>
                                    <h5 class="h5 pb-0 mb-0">Santosh R</h5>
                                    <div><small><i><img class="reviewlowo" src="img/veglyfelogo.png"> Bengaluru</i></small></div>
                                </article>
                              </div>
                              <div class="swiper-slide" data-swiper-autoplay="8000">
                                <article>
                                    <p class="text-center pb-0 mb-0"><span class="icon-quote-left icomoon h3"></span></p>
                                    <p>A lab with good service right from sample collection to delivering the report, descent staff. All my queries are taken care of.</p>
                                    <h5 class="h5 pb-0 mb-0">Sailaja Setti</h5>
                                    <div><small><i>Maha Lakshmi Foods</i></small></div>
                                </article>
                              </div>                             
                                                      
                            </div>
                                <!-- Add Pagination -->
                                <div class="swiper-pagination"></div>                          
                          </div>
                        <!--/ swiper -->
                    </div>
                    <!--/col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ white box -->
        </div>
        <!--/ container -->        
    </div>
    <!--/ testimonials-->

    <!-- client tele -->
    <div class="clients whitebox">
        <!-- container -->
        <div class="container">
           <!-- title -->
           <articler class="home-title text-center aos-item" data-aos="fade-up">              
                <h3><span>Clients that Trust Us</span></h3>
            </articler>
        <!--/ title -->

        <!-- clients in -->
        <div class="swiper-container clients-in">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src="img/client01.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client02.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client03.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client04.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client05.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client06.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client07.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client08.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client09.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client10.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client11.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client17.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client18.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client19.jpg" alt=""></div>
                <div class="swiper-slide"><img src="img/client20.jpg" alt=""></div>             
            </div>  
             <!-- Add Pagination -->
            <div class="swiper-pagination"></div>         
          </div>
        <!--/ clients in -->
        </div>
        <!--/ container -->
    </div>
    <!--/ client tele -->
    </main>

    <?php include 'footer.php'?>
   
    <?php include 'scripts.php' ?>


    <!-- video popup-->
    <!-- Modal -->
    <div class="modal fade" id="prewel-video" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Prewellabs</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <iframe id="ytplayer" mute="1" class="modalvideo" width="100%" height="460px" src="https://www.youtube.com/embed/aQjUAaus0sM" allow="autoplay" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>               
            </div>
            
        </div>
        </div>
    </div>
    <!--/ video popup-->

    
<script>
      //on click stop video for Home page
      $('#prewel-video').on('hidden.bs.modal', function (e) {
      $('#prewel-video iframe').attr('src', '');
      });

      $('#prewel-video').on('show.bs.modal', function (e) {
      $('#prewel-video iframe').attr('src', 'https://www.youtube.com/embed/aQjUAaus0sM?rel=0&amp;autoplay=1');
      });
  </script>


</body>
</html>
