<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs</title>  
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Pharma Testing</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Testing </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Pharma Testing</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->
    <!-- sub page body-->
    <div class="subpage-body">

    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row py-3">
            <!-- col -->
            <div class="col-lg-6 col-md-6 align-self-center aos-item" data-aos="fade-down">
                <img src="img/pharmatesting.jpg" alt="" class="img-fluid">
            </div>
            <!--/col-->                   
            <!--col-->
            <div class="col-lg-6 col-md-6 align-self-center aos-item" data-aos="fade-up">
                <h2>Why Pharma Testing has become important?</h2>
                <p>The pharmaceutical industry is significant for public health because it innovates, develops, and supplies medicine and medical equipment. What better example than the coronavirus pandemic! The world has increased emphasis on the pharma industry for developing potential solutions to combat the virus. The pharma sector plays a crucial role in discovering products for the health sector.</p>

                <p>When a new pharma product is discovered, it needs to undergo a series of tests before entering the market. This process, known as pharma testing, ensures safe use or consumption of pharma products. Besides safety, pharma testing is conducted for enhancing the efficacy of the products. Product efficiency is confirmed by implementing clinal tests on animal subjects first, followed by human testing. The efficacy is determined by analyzing the response. Pharma testing also helps to identify the dose requirement for administering the respective treatment. It also guarantees the delivery of quality products to the consumers.</p>
            </div>
            <!--col-->
        </div>
        <!--/ row -->       
    </div>
    <!--/ container -->   

        <!-- section -->
        <div class="whitebox py-2 py-md-5">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-8 text-center">
                        <h3>Various testing options that we provide:</h3>
                        <h5 class="h5 fblue">Microbiological Testing</h5>
                        <p class="text-center">Includes tests that enable identification of harmful microorganisms. They ensure safety and prevent any risks associated with microbes.</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
                
                <!-- row -->
                <div class="row justify-content-center pt-2 pt-sm-4">
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-test-tube icomoon"></span>
                        </div>  
                        <p> Sterility Testing </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-flask icomoon"></span>
                        </div>  
                        <p> Identification of Micro-Organisms </p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-virus icomoon"></span>
                        </div>  
                        <p> Bacterial Endotoxins Test </p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-allergy icomoon"></span>
                        </div>  
                        <p> Testing for Allergens </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-lab icomoon"></span>
                        </div>  
                        <p> Mycotoxin Testing </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-lab-1 icomoon"></span>
                        </div>  
                        <p> Biochemical Assay </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-microbiology icomoon"></span>
                        </div>  
                        <p> Microbial Assay </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-lab1 icomoon"></span>
                        </div>  
                        <p> Minimum Inhibitory Concentration (MIC) </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-science icomoon"></span>
                        </div>  
                        <p> Bioburden, TAMC and TYMC </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-bacterium icomoon"></span>
                        </div>  
                        <p> Disinfection </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-canned-food icomoon"></span>
                        </div>  
                        <p> Preservative Efficacy testing </p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
                <!-- row -->                 
            </div>
            <!--/ container -->
        </div>
        <!--/ sectioin --> 
    </div>
    <!-- sub page body -->    
    </main>
    <!--/ main ends -->
    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
