

//add class to header on scroll
$(window).scroll(function(){
    $(window).scroll(function () {
        if ($(this).scrollTop() > 25) {
            $('.fixed-top').addClass('fixed-theme');
        } else {
            $('.fixed-top').removeClass('fixed-theme');
        }
    });
});

//on click move to browser top
$(document).ready(function(){
  $(window).scroll(function(){
      if($(this).scrollTop() > 50){
          $('#moveTop').fadeIn()
      }else{
          $('#moveTop').fadeOut();
      }
  });

  //click event to scroll to top
  $('#moveTop').click(function(){
      $('html, body').animate({scrollTop:0}, 400);
  });
});




  //testimonaisl
  var swiper = new Swiper('.test-in', {
    speed: 900,
    disableOnInteraction: false,
    slidesPerView: 1,
    spaceBetween: 10,
    autoHeight:false,
    loop: false,
    autoplay: true,
    loop:true,
    // delay:20000,
    pagination: {
    el: '.swiper-pagination',
    clickable: true,
    },
    navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
    },
});

//clients 
var swiper = new Swiper('.clients-in', {
    speed: 300,
    slidesPerView: 1,
    spaceBetween: 10,   
    autoplay: true,
    autoHeight:false,
    // init: false,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      360: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      640: {
        slidesPerView: 3,
        spaceBetween: 20,
      },
      768: {
        slidesPerView: 4,
        spaceBetween: 40,
      },
      1024: {
        slidesPerView: 6,
        spaceBetween: 10,
      },
    }
  });





//lazy load
$(document).ready(
  function() {
    window.FakeLoader.init( { auto_hide: true } );
  }
);


//tooltip
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});






//main sider 
var swiper = new Swiper('.main-swiper', {
  spaceBetween: 0,
  centeredSlides: true,
  autoplay: {
    delay: 7000,
    disableOnInteraction: false,
    loop:true,
  },
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});


//disable right click on image
$(document).ready(function() {
  $(".img-fluid").on("contextmenu",function(){
  return false;
  }); 
});







   