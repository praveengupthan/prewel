<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs Food Testing</title>  
    <meta name="description" content="Not sure about your food quality? Call us at 08026589777 to get free consultation about how you can make sure your food is safe for consumption. We are NABL certified for food testing">
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Food Testing</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Testing </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Food Testing</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body-->
    <div class="subpage-body">
        <!--  whitebox -->        
            <div class="whitebox py-sm-3">
                <!-- container-->
                <div class="container">
                    <!-- row -->
                    <div class="row py-4">
                            
                    <!--col-->
                     <!-- col -->
                     <div class="col-lg-6 col-md-6 align-self-center aos-item" data-aos="fade-up">
                       <h2>Why Test Food ?</h2>
                       <p>The safety of the food remains a high priority for industry stakeholders, regulatory agencies and consumers. Due to the health and safety risks posed by chemical, microbiological and environmental contaminants, food testing is increasingly becoming a centerpiece of food safety programs. Public awareness of safety issues is quickly gaining ground. Recent events have been captured in news headlines, highlighting the diverse safety challenges posed by food contaminants.</p>
                    </div>
                    <!--col-->
                     <!-- col -->
                     <div class="col-lg-6 col-md-6 align-self-center">
                     <h2 class="text-center py-3">We Serve</h2>
                      <!-- row -->
                      <div class="row justify-content-center">
                            <!-- col -->
                            <div class="col-6 col-sm-3 col-md-6 icon-div aos-item" data-aos="fade-down">                               
                                <div class="icon">
                                    <span class="icon-school icomoon"></span>
                                </div>  
                                <p>Schools & Colleges</p>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-6 col-sm-3 col-md-6 icon-div aos-item" data-aos="fade-up">                               
                                <div class="icon">
                                    <span class="icon-factory icomoon"></span>
                                </div>  
                                <p>Companies and Industries</p>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                             <div class="col-6 col-sm-3 col-md-6 icon-div aos-item" data-aos="fade-down">                               
                                <div class="icon">
                                    <span class="icon-chef icomoon"></span>
                                </div>  
                                <p>Restaurants & Eateries</p>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                             <div class="col-6 col-sm-3 col-md-6 icon-div aos-item" data-aos="fade-up">                               
                                <div class="icon">
                                    <span class="icon-patient icomoon"></span>
                                </div>  
                                <p>Hospitals</p>
                            </div>
                            <!--/ col -->                            
                        </div>
                        <!--/ row -->                        
                    </div>
                    <!--col-->           
                </div>
                <!--/ row -->
                </div>
                <!-- container -->               
            </div>
            <!-- / container -->      

        <!-- section -->
        <div class="whitebox py-2 py-md-5">
            <!-- container -->
            <div class="container">
            <h3 class="text-center">Various Food testing options Prewel Labs provides</h3>

                <!-- row -->
                <div class="row justify-content-center pt-2 pt-sm-4">
                    <!-- col -->
                    <div class="col-6 col-lg-2 col-md-4  icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-research icomoon"></span>
                        </div>  
                        <p> Microbial testing </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-lg-2 col-md-4 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-bartender icomoon"></span>
                        </div>  
                        <p> Adulterant testing </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-lg-2 col-md-4 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-Vitamin icomoon"></span>
                        </div>  
                        <p> Nutritional value testing </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-lg-2 col-md-4 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-development-1 icomoon"></span>
                        </div>  
                        <p> Shelf life/stability studies </p>
                    </div>
                    <!--/ col -->    
                     <!-- col -->
                     <div class="col-6 col-lg-2 col-md-4 icon-div">                               
                        <div class="icon">
                            <span class="icon-learning icomoon"></span>
                        </div>  
                        <p> New product devlopment </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-lg-2 col-md-4 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-data-analysis icomoon"></span>
                        </div>  
                        <p> Hygiene Monitoring Program </p>
                    </div>
                    <!--/ col -->                
                </div>
                <!--/ row -->
                <!-- row -->
                
            </div>
            <!--/ container -->
        </div>
        <!--/ sectioin -->

        <!-- blue sub section -->
        <div class="benefits-section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-6 text-center">
                        <h4>Benefits of Food Testing</h4>                       
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
                <!-- row -->
                <div class="row justify-content-center py-2 my-sm-3">
                    <!--col-->
                    <div class="col-lg-3 col-md-6 benefits-col text-center aos-item" data-aos="fade-up">
                        <span class="icon-patient icomoon"></span>
                         <h6> For Consumers</h6>
                        <p> promises prevention from Foodborne illness</p>
                    </div>
                    <!--/ col -->
                     <!--col-->
                     <div class="col-lg-3 col-md-6 benefits-col text-center aos-item" data-aos="fade-down">
                        <span class="icon-factory icomoon"></span>
                        <h6> For Manufacturers</h6>
                        <p>benefits by assuring product safety thus accelerating their brand reputation.</p>
                    </div>
                    <!--/ col -->
                     <!--col-->
                     <div class="col-lg-3 col-md-6 benefits-col text-center aos-item" data-aos="fade-up">
                        <span class="icon-development-1 icomoon"></span>
                        <h6> For Corporates</h6>
                        <p>safeguards Employees health resulting in increased productivity.</p>
                    </div>
                    <!--/ col -->
                      <!--col-->
                      <div class="col-lg-3 col-md-6 benefits-col text-center aos-item" data-aos="fade-up">
                        <span class="icon-catering icomoon"></span>
                        <h6> For Caterers</h6>
                        <p> brings about positive brand recognition leading to increased business ventures.</p>
                    </div>
                    <!--/ col -->
                    
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ blue sub section -->

        <!-- container -->
        <div class="container whitebox highlets">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-4 col-sm-6 border-right highletscol aos-item" data-aos="fade-down">
                    <div class="d-flex justify-content-center">
                        <span class="icon-research icomoon"></span>
                        <div class="align-self-center">
                            <h5 class="fbold">1500+</h5>
                            <p class="fgray">Microbial testing</p>
                        </div>
                    </div>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-4 col-sm-6 border-right highletscol aos-item" data-aos="fade-up">
                    <div class="d-flex justify-content-center">
                        <span class="icon-bartender icomoon"></span>
                        <div class="align-self-center">
                            <h5 class="fbold">500+</h5>
                            <p class="fgray">Adulterant testing</p>
                        </div>
                    </div>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-4 col-sm-6 highletscol">
                    <div class="d-flex justify-content-center aos-item" data-aos="fade-down">
                        <span class="icon-Vitamin icomoon"></span>
                        <div class="align-self-center">
                            <h5 class="fbold">1000+</h5>
                            <p class="fgray">Nutritional testing</p>
                        </div>
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!-- sub page body -->
   

   

    
   
    

    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
