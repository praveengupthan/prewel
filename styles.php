<!-- style sheets -->
<link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bsnav.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/aos.css">
<link rel="stylesheet" href="css/swiper.min.css">
<link rel="stylesheet" href="css/icomoon.css">
<link rel="stylesheet" href="css/fakeloader.css">
<link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />


<!-- Global site tag (gtag.js) - Google Ads: 721965202 --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-721965202"></script> 
<script>
 window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-721965202'); 
</script>



<!-- Event snippet for Website sale conversion page --> 
<script> 
gtag('event', 'conversion', { 'send_to': 'AW-721965202/kgv2CKOQ2NgBEJKhodgC', 'transaction_id': '' }); 
</script>