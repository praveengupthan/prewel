<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs</title>  
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Clean Air Month</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>  
                         <li class="breadcrumb-item"><a href="blog.php">Blog</a></li>                             
                        <li class="breadcrumb-item active" aria-current="page">Clean Air Month</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body-->
    <div class="subpage-body">    
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- left content col -->
                <div class="col-lg-9 py-4">
                <figure>
                    <img src="img/blog/clean-air-month.jpg" alt="" class="img-fluid">
                        <p><small><span class="icon-calendar"></span> Posted on 17 May 2019 by Admin</small></p>
                    </figure>
                    <article>
                        <p>The month of May is finally here with summer now in full swing. But did you know, the month is also celebrated as the Clean Air Month, in a bid to create awareness about the importance of air quality and how to improve it? Given, that we spend most of the time indoors, reports say that it is around 90 percent of the time be it either in our homes or workplaces, shouldn’t it be an imperative task to monitor our indoor environment! But sadly, many of us are caught unaware of indoor air quality leading to severe to mild health complications. Indoor air pollutants can, in fact, acerbate conditions of asthma and in the long run, can cause respiratory illness such as chronic bronchitis or even complications of cancer. Alarming as it sounds, in this ‘Clean air month’, here are some details you should definitely know indoor air quality.</p> 

                        <h4>Significance of Indoor air quality</h4>

                        <p>Indoor air quality is a significant aspect one should always keep in check as it can impact your life in both the positive and negative sense with regards to health, mood, comfort, and productivity of its occupants. According to WHO (World Health Organization), microorganisms like fungi, mold, and bacteria present in and around the building served as a major indoor bio-pollutant which in turn, aggravated conditions of respiratory symptoms, allergies, asthma attack, etc. In a study carried across various countries such as North America, India, Australia, and Europe, Dampness, which was found to be present in more than 10- 50 percent of indoor homes, was a major factor triggering microorganism growth. Other independent studies conducted by different science groups also threw light on a shocking revelation that the level of pollutants indoors preceded those on the outside.</p>

                        <h4>Indoor Air pollutants</h4>

                        <p>To minimize risks from indoor air pollution, it is fundamental to understand the different pollutants that commonly inhabit our homes</p>

                        <h5 class="h5">Chemical contaminants</h5>

                        <p>The VOCs or Volatile organic compounds involve a range of both manmade and natural compounds which easily pollute the air quality. It can be smoke from tobacco, emissions from furniture, paint and flooring, varnish, perfumes, cleaning agents, carpet dyes and fibers, etc. Radon, a highly carcinogenic gas which is also the leading runner for lung cancer, can be released from the decay of uranium in soils, which then can seep into homes through cracks in foundations or creaky basements. Formaldehyde is another such pollutant that can be emitted from furniture, cabinetry, foam insulation and other forms of plywood. Emissions from pesticides such as bug sprays and other disinfectants can also harm your indoor air.</p>

                        <h5 class="h5">Particle contaminants</h5>

                        <p>This category involves substances that can be in either solid or liquid or in a combined state such that they remain suspended in the air. Though most are big in size making it impossible to be drawn into our body through respiration, some small ones around 7 microns do get through, which on the long run can severely impede your health. Dust, dirt particles, wood shavings, lead from carpets or dirt, etc. are some of the particles which can easily swarm your homes at doing of tasks such as remodeling, wood workmanship, or even simple menial tasks like dusting, cleaning, printing or using other related equipment, etc.</p>

                        <h5 class="h5">Biological contaminants</h5>

                        <p>The category entails contaminants like bacteria, mold, dust, pollen, animal dander, pests, fibers, etc. Which may be the aftermath of poor maintenance and housekeeping. Water leakage, seepage, poor ventilation, and humidity control are some of the main causes, followed by those brought in by the building occupants and pets, etc. Allergies are the common ailment that plagues the occupants, though each can vary largely with individuals. These contaminants also play an essential role in triggering symptoms of asthma.</p>

                        <p><span class="fbold">Health impeachments </span> from indoor air pollutants can fall under two categories- the short term effects and long term effects.</p>

                        <ul class="list-items">
                            <li>1. The short term effects that may entail a person on a single or repeated exposure can be skin, eye or nose irritations, headache, dizziness, body fatigue coughing, upper body congestion, etc. Normally removing the occupant from the source of pollution can be a treatment whilst in other cases, medications may follow. These effects however largely depend on the individual, their age, immunity, and sensitivity to the particular contaminant or so.</li>
                            <li>2. The Long term effects involve those that only begin to show after years or long period of repeated exposures to the pollutant. From respiratory illness like lung disease, Dyspnea to heart ailments and even cancer, the effects of long term exposure can be extremely harmful and in certain cases, even fatal. The suspected cases of cancer due to impure indoor air quality have scientists ranking indoor air pollution as one of the most significant environmental pollutions.</li>
                        </ul>

                        <p>IAQ Parameters we perform</p>

                        <ul class="list-items">
                            <li>Carbon Dioxide</li>
                            <li>Carbon Monoxide</li>
                            <li>Temperature</li>
                            <li>Relative Humidity</li>
                            <li>Ozone</li>
                            <li>Oxygen</li>
                            <li>Hydrogen Sulphide</li>
                            <li>Total Volatile Organic Compounds (60 Compound)</li>
                            <li>Formaldehyde</li>
                            <li>Nitrogen Dioxide</li>
                            <li>Ammonia</li>
                            <li>Suspended Articulate Matter</li>
                            <li>Sulphur Dioxide</li>
                        </ul>

                        <p>Getting to know your indoor air quality is prerequisite to preventing any further damage from indoor air pollutants. At Incepbio, we are aimed at preventive wellness and validation, we take utmost care and precision to test your indoor air for a variety of pollutants and allergens using highly efficient calibration methods. From testing for volatile compounds, we are equipped to handle any pollutants which aren’t easy to detect. With your air tested, you can now implement ways of reducing the contaminants and living a full and happy life. To know more, do refer to our website.</p> 

                        <a href="get-schedule.php" class="btn btn-outline-secondary">Book Test <span class="icon-arrow-right"></span> </a>
                    </article>            
                       
                    
                </div>
                <!--/ left content col -->
                 <?php include 'recentblogs.php' ?>
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->        
    </div>
    <!-- sub page body -->
    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
