<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs Certification</title>  
    <meta name="description" content="You can view our NABL certificate conforming to ISO/IEC 1705:2017 in the field of testing on this page">
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Certifications</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page">Certifications</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body-->
    <div class="subpage-body">       
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row py-md-4 justify-content-center">
                <!-- col -->
                <div class="col-md-8 aos-item" data-aos="fade-up">
                    <div class="card">                        
                        <div class="card-body">
                                <img src="img/Certificateof-Accreditation.jpg" class="img-fluid certification-img">
                            <a href="img/pdf/Certificateof Accreditation.pdf" target="_blank" class="fblue d-block text-right"><span class="icon-cloud-download"></span> Download</a>
                        </div>
                    </div>
                </div>
                <!--/ col -->                
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->      
    </div>
    <!-- sub page body -->
    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
