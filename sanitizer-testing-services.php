<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs</title>  
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Sanitizer Testing Services</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Testing </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Sanitizer Testing Services</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->
    <!-- sub page body-->
    <div class="subpage-body">

        <!--  whitebox -->        
        <div class="whitebox py-sm-3">
            <!-- container-->
            <div class="container">
                <!-- row -->
                <div class="row py-3">
                    <!-- col -->
                    <div class="col-lg-6 col-md-6 align-self-center aos-item" data-aos="fade-down">
                        <img src="img/hand-sanitizers.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/col-->                   
                    <!--col-->
                    <div class="col-lg-6 col-md-6 align-self-center aos-item" data-aos="fade-up">
                        <h3 class="h3 pb-2">Covid-19 | Hand Sanitizers | Testing and Certification services </h3>
                        <h4 class="h5">Hand Sanitizers</h4>
                        <p>Hand sanitizers or hand rubs are a means of rapidly and effectively deactivating a broad-spectrum harmful microorganism on hands and surfaces.</p>
                        <p><b>Chemical disinfection </b> refers to the reduction of the number of microorganisms in or on a matrix, this is achieved by irreversible action to structure or metabolism of a product, to a level judged to be appropriate for a defined purpose.</p>
                        <p>Disinfectants do not necessarily kill all organisms (sterilisation) but reduce them to a harmless level for human and/or animal health.</p>    
                        <p>Disinfection claims relating to microorganisms we refer to: vegetative bacteria, yeasts, mould spores, bacterial spores, and viruses.</p>    
                        <p><b>Hand sanitizers, gels and rubs</b> can be categorised as cosmetics or biocide products, depending on the product’s primary function, claims, ingredients and intended use. </p>                
                    </div>
                    <!--col-->
                    <div class="col-12">
                        <p><b>Cosmetics products </b> are designed to primarily clean and / or moisturise the skin. When categorised as cosmetics, the product would be referred to as a hand rub or hand gel and would fall within the cosmetics regulations with efficacy testing required to support claims.  </p>
                    </div>
                </div>
                <!--/ row -->
            </div>
            <!-- container -->        
        </div>
        <!-- / Whitebox -->

        <!-- container -->
        <div class="container">           
            <!-- row -->
            <div class="row py-5">
                <!-- col -->
                <div class="col-lg-12">
                    <h3 class="aos-item pb-2" data-aos="fade-down">Test methods and requirements for hand hygiene products Basic quality control </h3>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 aos-item" data-aos="fade-down">                  
                    <ul class="list-items-noicon bglist">
                        <li>Chemical product appearance – Visual examination</li>
                        <li>Weight or Volume – Gravimetry</li>
                        <li>Density at 20 °C – PE 2.2.5</li>
                        <li>pH – Potentiometry</li>
                        <li>Ethanol – GC/FID Default variation; Digital photography – Default variation</li>
                        <li>Glycerol</li>
                        <li>Isopropanol</li>
                                      
                    </ul>                   
                </div>
                <!-- /col -->
                <!-- col -->
                <div class="col-lg-6 aos-item" data-aos="fade-up">                      
                    <ul class="list-items-noicon bglist">
                        <li>“Brookfield viscosity” test</li>
                        <li>“H2O2” dosage</li>     
                        <li>Alcohol concentration of raw materials and final product</li>
                        <li>Additives – Should be as non-toxic as possible (considering accidental ingestion)</li>
                        <li>Perfumes or dyes – Not recommended, in order to avoid risk of allergic reactions</li>
                        <li>Labelling – In accordance with national and international regulations</li>
                        <li>Flammability</li>                            
                    </ul>      
                </div>
                <!-- /col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->

         <!--  whitebox -->        
         <div class="whitebox py-sm-3">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-5">                  
                    <!-- col -->
                    <div class="col-lg-6 col-md-6 aos-item" data-aos="fade-up"> 
                           
                        <h4 class="pt-3 pt-sm-1">Test methods</h4>
                        <p>Dependent on a product’s classification and claims, some specific test methods should applied, the following tests are required for disinfectants:</p>

                        <ul class="list-items-noicon bglist">
                            <li>Germ kill efficacy test</li>
                            <li>Hand rub and hand wash studies</li>
                            <li>Minimum Inhibitory Concentration (MIC)</li>
                            <li>Virucidal activity versus standard and specific human viruses including Betacoronavirus models</li>
                            <li>Virucidal activity, Clinical trial studies as per CDSO, DCGI and ICMR</li>
                        </ul>                            
                    </div>
                    <!-- /col -->


                    <!-- col -->
                    <div class="col-lg-6 col-md-6 aos-item" data-aos="fade-down">                           
                        <h4 class="pt-3 pt-sm-1">Efficacy testing standards</h4>

                        <div class="table-responsive">
                            <table class="table table-bordered cust-table">
                                <thead>
                                    <tr>
                                        <th width="40%">Description</th>
                                        <th width="60%">Guidelines</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Virucidal</td>
                                        <td>EN 14476, ASTM E1052</td>
                                    </tr>
                                    <tr>
                                        <td>Virucidal on Corona +ve patients</td>
                                        <td>Clinical trial studies as per CDSO, DCGI and ICMR</td>
                                    </tr>
                                    <tr>
                                        <td>Fungicidal / yeasticidal </td>
                                        <td>EN 1650, EN 13624, ASTM E2613</td>
                                    </tr>
                                    <tr>
                                        <td>Bactericidal activity  </td>
                                        <td>EN 1276, EN 14348, EN 13727, EN 1500, EN 1040, EN 1499, ASTM E2752, ASTM E2755, ASTM E1174, ASTM E1115, ASTM E2315</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                      
                    </div>
                    <!-- /col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
         </div>
         <!--/ white box -->
    </div>
    <!-- sub page body -->    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
