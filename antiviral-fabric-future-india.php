<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Antiviral Fabric - Does it have a Future in India</title>  
    <meta name="description" content="Antiviral fabrics have a coating that can prevent viruses like covid-19, from attaching to them. The Indian Industry is starting to adopt this fabric.">
    <meta name="keyword" content="Antiviral Fabric">
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
           <!-- row -->
           <div class="row justify-content-center">
                <!-- col -->
                <div class="col-lg-7">
                    <article>
                        <h1>Antiviral Fabric - Does it have a Future in India?</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>  
                                <li class="breadcrumb-item"><a href="blog.php">Blog</a></li>                             
                                <li class="breadcrumb-item active" aria-current="page">Antiviral Fabric - Does it have a Future in India?</li>
                            </ol>
                        </nav>
                    </article>
                </div>
                <!--/ col -->
           </div>
           <!--/row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body-->
    <div class="subpage-body blog-detail">    
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- left content col -->
                <div class="col-lg-7 py-4">
                    <article>
                        <h2>Why use Antiviral fabric?</h2>
                        <p>During the coronavirus outbreak, PPE such as masks, gowns, and gloves gained emphasis. People were rushing to buy them, which resulted in a high demand and less supply. Probably, not many are aware that PPE and textiles can also be sources of infection spread.  Therefore, the world needs reusable PPE to meet the increasing need for virus protection. This is where the concept of antiviral fabric comes into play. Such a technology would allow the reuse of PPE and help solve their crisis. It remains unclear whether or not coronavirus can spread through clothes and textiles, but it is quite a possibility. In that case, the antiviral fabric could be an ideal solution for the pandemic.</p> 

                        <h2>How does the Antiviral Fabric work?</h2>
                        <p>Antiviral fabrics have a coating that can prevent viruses, including covid-19, from attaching to them. The Antiviral textile coating is durable so that it can remain stable after multiple washes. Reusable hospital textiles can be made with this coating and it would allow for repeated washing and sanitization. Since the textile coating is reusable, it can be used in PPE, thereby making them last for long durations. The coating developed by HeiQ is produced by combining silver technology and vesicle technology. This coating can also be applied to woolen fabric.</p>

                        <h2>What are the characteristics of the virus-resistant fabric and products?</h2>
                        <p>The fabric developed by Donear group and Grado undergoes tests for determining their resistance to contaminated objects, liquid aerosols, and pressurized liquids. It can sustain 50 washes and is designed for daily use in the form of different garments. Like this fabric, the one developed by Fabiosys innovations also retains its anti-viral characteristics after several washes. It is an affordable technology that transforms cotton fabric into a fabric that protects against infections, particularly hospital-acquired infections. </p>

                        <p>The fabric technology developed by the Coimbatore company has its application on fabrics that kills or inactivates the virus in 3 minutes of contact. The Viral Shield line is the first reusable PPE around the globe that has successfully cleared several tests including the viral penetration test because it consists of a specialized virus PU barrier film.</p>    
                        
                        <h2>Are there any standards for the anti-viral products?</h2>
                        <p>The ISO 18184:2019 entails the standards for validating the quality and effectiveness of antiviral textiles. These, however, are the international standards for testing the anti-viral activity of textiles. Whether or not enough standards have been established for anti-viral products remains a question, especially in India.</p>

                        <p>While the Indian manufacturers of this fabric strongly believe it can protect people against the pandemic-causing coronavirus, the Indian government is yet to establish guidelines for it. Some anti-viral textiles can be expensive, costing 15-20% more than the average ones. Moreover, Indian officials have argued about its effectiveness against covid-19, adding that the WHO fails to recognize the fabric.</p>
                       
                        <h2>Which companies are involved in Virus repellent fabric manufacturing?</h2>
                        <p>HeiQ is one of the leading companies involved in producing a range of anti-viral and anti-bacterial fabric. Furthermore, the Albini Group developed ViroFormula fabrics in partnership with HeiQ. The LAMP lab at the University of Pittsburgh Swanson School of Engineering also discovered a textile coating which is capable of preventing viruses from attaching. The anti-viral property of this coating is retained despite washing or scrubbing. It demonstrated effectiveness against adenoviruses. Tests are ongoing to confirm whether it can repel covid-19. The Grado company launched a fabric that is anti-viral and anti-bacterial. </p>

                        <h2>How Indian companies are stepping in?</h2>
                        <p>HeiQ ViroBlock NPJ03 textile technology is among the first ones that have been certified as effective against coronavirus. Besides international organizations, Indian companies are being actively involved. </p>
                        <p>Donear Group partnered with HEIQ in Zurich (Switzerland) ViroBlock launched a high-grade, anti-bacterial, and anti-viral fabric. The neo-tech fabric has been tested for ISO 18184 rapid test among many others. </p>
                        <p>Grado utilizes neo-tech technology developed by Donear industries to produce a virus-repellant fabric. The fabric has a protective shield to protect against viruses and bacteria. It has received certification from laboratories including NABL-accredited biotech service providers. </p>
                        <p>The Fabiosys Innovations at Indian Institute of Technology (IIT)-Delhi also produced a fabric that is infection-proof. </p>
                        <p>Additionally, a textile company located in Coimbatore has declared that it has manufactured an anti-viral fabric against the spread of coronavirus. It has created an anti-virus technology named ViroBlock in collaboration with a Swiss company to innovate the fabric. </p>
                        <p>Arvind Ltd. entered in partnership with HeiQ and Taiwanese company Jintex Corporation regarding anti-viral textile called ViroBlock under the brand Intellifabrix. </p>
                        <p>Zodiac Clothing Co.Ltd launched its anti-viral shirt having the brand name Securo. </p>
                        <p>Loyal Textile Mills launched a PPE product line named Viral Shield in partnership with Reliance Industries, India, and HeiQ from Switzerland. </p>
                        <p>Vardhaman Textiles and D’Decor are amongst other textile manufacturers promoting new fabrics as a solution against covid-19.</p>

                        <h2>The final take on anti-viral textiles?</h2>
                        <p>The entire world has been struggling and living in fear during the ongoing coronavirus pandemic. In times like this, various companies are taking initiative to invent technologies and products that would help protect against the virus. </p>

                        <p>PPE has been widely used as a recommended protection to a large extent. The requirement has been so high that there has been a crisis, mainly in hospitals where it is needed the most. The source of transmission for covid-19 is still under review and textiles could very well be a source. Normal textiles may retain viruses and transmit them in places like hospitals. That is why industrial manufactures started launching anti-viral textiles. It was initially developed by HeiQ, but now many Indian industries have participated in the venture. </p>
                        <p>However, its use and effectiveness are raising debates, particularly in terms of the standards. Although ISO standards have been established for anti-viral textiles, the Indian government and the WHO are uncertain if those apply against covid-19. There are doubts as to whether virus fighting textiles are an ideal solution for covid-19. Perhaps, only time will tell whether anti-viral PPE is a worthy investment, and if or not it will help in the fight against the pandemic.</p>
                        <p>On a similar note Prewel Labs also strives to safeguard the human life from all kinds diseases caused by unhygienic water, food or low quality indoor air. To take the fight forward against this pandemic caused by the coronavirus. Prewel Labs is offering <span class="fbold">Antiviral Fabric Testing services </span> by conducting <span class="fbold">clinical studies.</span></p>

                       

                        <a href="contact.php" class="btn btn-outline-secondary">Book Test <span class="icon-arrow-right"></span> </a>
                    </article>
                    
                </div>
                <!--/ left content col -->
                
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->        
    </div>
    <!-- sub page body -->
    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
