<!-- footer -->
<footer>
        <a href="javascript:void(0)" class="moveTop" id="moveTop"><span class="icon-chevron-up icomoon"></span></a>
        <!-- container -->
        <div class="container">
            <ul class="nav justify-content-center">
                <li class="nav-item"><a href="http://prewellabs.com/" class="nav-link">Home</a></li>
                <li class="nav-item"><a href="consultation.php" class="nav-link">Consultation</a></li>
                <li class="nav-item"><a href="solutions.php" class="nav-link">Solutions</a></li>
                <li class="nav-item"><a href="certifications.php" class="nav-link">Certifications</a></li>
                <li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li>
                <li class="nav-item"><a href="terms.php" class="nav-link">Terms &amp; Conditions </a></li>
                <li class="nav-item"><a href="privacy.php" class="nav-link">Privacy Policy</a></li>
            </ul>
           <div class="footer-social text-center">
               <a href="https://www.facebook.com/prewellabs/" target="_blank"><span class="icon-facebook icomoon"></span></a>
               <a href="https://www.instagram.com/prewellabs/" target="_blank"><span class="icon-instagram icomoon"></span></a>
               <a href="https://www.linkedin.com/company/prewel-labs/" target="_blank"><span class="icon-linkedin icomoon"></span></a>
           </div>
           <p class="text-center fwhite">
               <i><small>Copyright © 2020 Prewellabs. All rights reserved.</small></i>
           </p>
        </div>
        <!--/ container -->
    </footer>
    <!--/ footer -->

    <script type="text/javascript"> _linkedin_partner_id = "2195666"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=2195666&fmt=gif" /> </noscript>


        <!-- modal-->
        <div class="modal fade" id="product-more" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-slideout modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Schedule your Sample Pickup</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <!-- modal body-->
            <div class="modal-body">
          

                 <!-- form -->
                <form class="form" id="homeformEnquiry" action="" method="post" name="formEnquiry">
                    <!-- row -->
                    <div class="row">                       
                        <!-- col -->
                        <div class="col-md-12">
                            <div class="form-group">                               
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="Name of Person/Organisation" name="orgName" >
                                </div>
                            </div>
                        </div>
                        <!--/ col -->     
                        
                        <!-- col -->
                        <div class="col-md-12">
                            <div class="form-group">                               
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="Contact Number" name="phNumber">
                                </div>
                            </div>
                        </div>
                        <!--/ col --> 

                        <!-- col -->
                        <div class="col-md-12">
                            <div class="form-group">                               
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="Email Address" name="mailId">
                                </div>
                            </div>
                        </div>
                        <!--/ col --> 

                        <!-- col -->
                        <div class="col-md-12">
                            <div class="form-group">                               
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="Sample Pickup Address" name="pickUpAddress">
                                </div>
                            </div>
                        </div>
                        <!--/ col --> 
                        
                        <!-- col -->
                        <div class="col-md-12">
                            <div class="form-group">                               
                                <div class="input-group">
                                <select class="form-control" name="selService" >
                                    <option value="">Select Service</option>
                                    <option value="Water Testing">Water Testing</option>
                                    <option value="Food Testing">Food Testing</option>
                                    <option value="Air Testing">Air Testing</option>
                                    <option value="Medical Device Testing">Medical Device Testing</option>
                                    <option value="Environmental Testing">Environmental Testing</option>
                                    <option value="Pharma Testing">Pharma Testing</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <!--/ col --> 

                        <!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">                               
                                <div class="input-group">
                                    <input class="form-control"  id="datepicker" placeholder="Date of Pickup" name="dtofPickup">
                                </div>
                            </div>
                        </div>
                        <!--/ col --> 

                        <!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">                              
                                <div class="input-group">
                                <select class="form-control" name="timeSchedule">
                                    <option value="">Select Time Schedule</option>
                                    <option value="9AM - 11AM">9AM - 11AM</option>
                                    <option value="11AM - 1PM">11AM - 1PM</option>
                                    <option value="1PM - 3PM">1PM - 3PM</option>
                                    <option value="3PM - 5PM">3PM - 5PM</option>
                                    <option value="5PM - 7PM">5PM - 7PM</option>                               
                                </select>
                                </div>
                            </div>
                        </div>
                        <!--/ col --> 

                        <!-- col -->
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Message</label>
                                <div class="input-group">
                                <textarea style="height:120px;" class="form-control" placeholder="Write Message" name="msg"></textarea>
                                </div>
                            </div>
                        </div>
                        <!--/ col --> 

                        <!-- col -->
                        <div class="col-lg-12">
                            <input  type="submit" class="btn btn-primary" value="Submit" name="submitEnquiry" >
                        </div>
                        <!--/ col --> 
                    </div>
                    <!-- row -->
                </form>
                <!--/ form -->    
            </div>         
            <!--/ modal body --> 
          </div>
        </div>
      </div>
      <!--/ model -->