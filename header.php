<!-- header -->
<header class="fixed-top">
        <!-- custom container -->
        <div class="cust-container">
            <div class="navbar navbar-expand-lg bsnav  px-md-0">
                <a class="navbar-brand" href="http://prewellabs.com/">
                    <img src="img/logo.svg" alt="">
                </a>
                <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse justify-content-md-end mt-md-2">
                  <ul class="navbar-nav navbar-mobile mr-0">
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'active';}else {echo'nav-link';} ?>" href="http://prewellabs.com/">Home</a></li>                   
                    <li class="nav-item dropdown zoom">
                      <a class="<?php if(
                        basename($_SERVER[ 'SCRIPT_NAME'])=='water-testing.php' || 
                        basename($_SERVER[ 'SCRIPT_NAME'])=='food-testing.php' || 
                        basename($_SERVER[ 'SCRIPT_NAME'])=='air-testing.php' ||                        
                        basename($_SERVER[ 'SCRIPT_NAME'])=='environmentaltesting.php'||
                        basename($_SERVER[ 'SCRIPT_NAME'])=='medical-device-testing.php'||
                        basename($_SERVER[ 'SCRIPT_NAME'])=='pharma-testing.php'||
                        basename($_SERVER[ 'SCRIPT_NAME'])=='hand-sanitizer-testing.php'
                        ) {echo 'active'; } else {echo ''; }?> nav-link" href="#">Testing <i class="caret"></i></a>
                      <ul class="navbar-nav submenu">
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'water-testing.php'){echo 'active'; }else { echo 'nav-link'; } ?>" href="water-testing.php">Water Testing</a></li>                       
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'food-testing.php'){echo 'active'; }else { echo 'nav-link'; } ?>" href="food-testing.php">Food Testing</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'air-testing.php'){echo 'active'; }else { echo 'nav-link'; } ?>" href="air-testing.php">Air Testing</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'environmentaltesting.php'){echo 'active'; }else { echo 'nav-link'; } ?>" href="environmentaltesting.php">Environmental Testing</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'medical-device-testing.php'){echo 'active'; }else { echo 'nav-link'; } ?>" href="medical-device-testing.php">Medical Device Testing</a></li>                        
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'pharma-testing.php'){echo 'active'; }else { echo 'nav-link'; } ?>" href="pharma-testing.php">Pharma Testing</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'sanitizer-testing-services.php'){echo 'active'; }else { echo 'nav-link'; } ?>" href="sanitizer-testing-services.php">Sanitizer Testing Services</a></li>
                      </ul>                      
                    </li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='consultation.php'){echo'active';}else {echo'nav-link';} ?>" href="consultation.php">Consultation</a></li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='solutions.php'){echo'active';}else {echo'nav-link';} ?>" href="solutions.php">Solutions</a></li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='certifications.php'){echo'active';}else {echo'nav-link';} ?>" href="certifications.php">Certifications</a></li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='blog.php'){echo'active';}else {echo'nav-link';} ?>" href="blog.php">Blog</a></li>                     
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'active';}else {echo'nav-link';} ?>" href="contact.php">Contact</a></li>   
                    <li class="nav-item"><a class="nav-link spl-link" href="javascript:void(0)" data-toggle="modal" data-target="#product-more">Free Sample Pickup</a></li>                
                  </ul>
                </div>
              </div>           
             
            <div class="bsnav-mobile">
                <div class="bsnav-mobile-overlay"></div>
                <div class="navbar"></div>
            </div>
        </div>
        <!--/ custom container-->

    </header>
    <!--/ header ends -->
    <!-- Facebook Pixel Code -->
    <?php     

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if(isset($_POST['submitEnquiry'])){
$to = "support@prewellabs.com"; 
$subject = "Mail From ".$_POST['orgName'];
$message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<p>".$_POST['orgName']." has sent mail!</p>
<table>
<tr>
<th align='left'>Organization Name</th>
<td>".$_POST['orgName']."</td>
</tr>
<tr>
<th align='left'>Contact Number</th>
<td>".$_POST['phNumber']."</td>
</tr>
<tr>
<th align='left'>Email</th>
<td>".$_POST['mailId']."</td>
</tr>
<tr>
<th align='left'>Pickup Address</th>
<td>".$_POST['pickUpAddress']."</td>
</tr>
<tr>
<th align='left'>Required Service</th>
<td>".$_POST['selService']."</td>
</tr>
<tr>
<th align='left'>Date of Pickup</th>
<td>".$_POST['dtofPickup']."</td>
</tr>
<tr>
<th align='left'>Time of Schedule</th>
<td>".$_POST['timeSchedule']."</td>
</tr>
<tr>
<th align='left'>Message</th>
<td>".$_POST['msg']."</td>
</tr>
</table>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From:' .$_POST['orgName']. "\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

mail($to,$subject,$message,$headers);    

?>
<div class="alert alert-success alert-dismissible fade show" role="alert" id="alertEnquirySuccess">
  Your Enquiry Sent. Thank you <?= $_POST['orgName'] ?>, we will contact you shortly.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<?php
// echo "<p style='color:green'>Mail Sent. Thank you " . $_POST['name'] . ", we will contact you shortly.</p>";
}
?>




<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)}; if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');fbq('init', '805746943289928'); fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" src="https://www.facebook.com/tr?id=805746943289928&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171769409-1"></script>
<script>
  window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-171769409-1');
</script>




