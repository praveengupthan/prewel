<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs Terms</title>  
    <meta name="description" content="For more information about our terms of use, or if you have a complaint, please contact us by e-mail at support@incepbio.com">
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Terms</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page">Terms</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->
    <!-- sub page body-->
    <div class="subpage-body py-4">

    <!-- container -->
    <div class="container">

    <h3>Prewellabs Website Usage Terms and Conditions</h3>
    <p>Welcome to our website. If you continue to browse and use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern IncepBio relationship with you in relation to this website. If you disagree with any part of these terms and conditions, please do not use our website.</p>
        
    <p>The terms “IncepBio” or “IncepBio Pvt.Ltd or “Us” or “We” refers to the owner of the website whose registered office is No. 24, 22nd Main, Marenahalli, J P Nagar 2nd Phase, Bengaluru-560078</p>

    <p>Our company incorporation number is U24 304KA2016PTC094041. The term ‘you’ refers to the user or viewer of our website.</p>

    <h5>The use of this website is subject to the following terms of use:</h5>
    <p>The content of the pages of this website is for your general information and use only. It is subject to change without notice.</p>
    <p>This website uses cookies to monitor browsing preferences. If you do allow cookies to be used, the following personal information may be stored by us for use by third parties: name, location, membership number, IP address and browsing history.</p>

    <p>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</p>

    <p>Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</p>

    <p>This website contains material which is owned by or licensed to us. This material includes, but is not limited to design, layout, look, appearance, photos and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</p>

    <p>All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website.</p>

    <p>Unauthorized use of this website may give rise to a claim for damages and/or be a criminal offence.</p>

    <p>From time to time, this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s). Some information provided on this Website has been obtained from third parties and is provided for informational purposes only. We are not liable for any inaccuracies in any third-party information provided on this Web Site and does not endorse any of the activities, guides, vendors or service providers described. It is your responsibility to investigate the safety and suitability of any activity, and the credentials and fitness of any third-party guide, vendor or service provider. We expressly deny any liability for engagement in any activity, and for use of any third-party guide, vendor or service provider that may be mentioned or described on this Website. Additional fees, terms and conditions, and restrictions may apply to any activity or service and are determined solely by the third-party vendor, guide, or service provider. Please contact the third-party service provider, guide or vendor for complete details. We do not book, nor do we make any representations regarding the availability of third party activities or services.</p>

    <p>Your use of this website and any dispute arising out of such use of the website is subject to the laws of India.</p>

    <p>Words and expressions used in these website usage terms and conditions shall bear the ordinary meaning assigned to them unless the context dictates otherwise</p>

    <h3>Contact us</h3>
    <p>For more information about our terms of use, if you have questions, or if you would like to make a complaint, please contact us by e-mail at <a href="mailto:support@prewellabs.com">support@prewellabs.com</a> or by mail using the details provided below:</p>

    <h3>Prewel Labs</h3>
    <p>No. 24, 22nd Main, Marenahalli, J P Nagar 2nd Phase, Bengaluru-560078</p>

    <h3>Update</h3>
    <p>
        <i>This document on terms of use was last updated on: July 22, 2020.  Should we update, amend or make any changes to our terms of use conditions, those changes will be posted here.</i>
    </p>

        

       
    </div>
    <!--/ container -->
    
    </div>
    <!-- sub page body -->    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
