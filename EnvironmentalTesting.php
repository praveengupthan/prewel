<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs</title>  
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Environmental Testing</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Testing </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Environmental Testing</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->
    <!-- sub page body-->
    <div class="subpage-body">

        <!--  whitebox -->        
        <div class="whitebox py-sm-3">
            <!-- container-->
            <div class="container">
                <!-- row -->
                <div class="row py-3">
                    <!-- col -->
                    <div class="col-lg-6 col-md-6 align-self-center aos-item" data-aos="fade-down">
                        <img src="img/environment-testing.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/col-->                   
                    <!--col-->
                    <div class="col-lg-6 col-md-6 align-self-center aos-item" data-aos="fade-up">
                        <h3 class="h3 pb-2">Why you should Opt for Environmental Testing?</h3>
                        <p>The environment that we live in determines our health and overall well-being. Environmental testing helps to identify hazardous components present in the environment to ensure safe and healthy living. For this reason, environmental testing becomes a mandatory requirement for governments. Compulsory environmental testing would guarantee that providing a clean environment becomes the government’s main agenda, which would help the citizens live a high-quality life. Prewel Labs partakes in this initiative to create a clean and healthy environment for living. We provide a variety of laboratory testing and consultancy services to a wide range of corporates, industries, environmental consultants, as well as government authorities.</p>
                    </div>
                    <!--col-->
                </div>
                <!--/ row -->
            </div>
            <!-- container -->        
        </div>
        <!-- / Whitebox -->

        <!-- container -->
        <div class="container">           
            <!-- row -->
            <div class="row py-5">
                <!-- col -->
                <div class="col-lg-12">
                    <h3 class="aos-item pb-2" data-aos="fade-down">The Benefits that You get from Environmental Testing</h3>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 aos-item" data-aos="fade-down">                  
                    <p>Our environment indicates the surrounding we stay in. In order to live a long and healthy life, the quality of the environment and the air we breathe is important. With industrialization and technological advancement, air pollutants are released in large quantities from various sources. These pollutants tend to persist in the atmosphere and pose harm to human health. Carbon dioxide, methane, water vapor, nitrous oxide, and ozone, known as greenhouse gases, cause global warming. The testing of environments for monitoring hazardous pollutants is a rare occurrence. Environmental testing is important because it determines the presence of pollutants in the atmosphere. </p>
                   
                </div>
                <!-- /col -->
                <!-- col -->
                <div class="col-lg-6 aos-item" data-aos="fade-up">                      
                    <p> Once the potentially hazardous constituents are identified, suitable mitigation measures can be implemented.  Environmental testing also protects the health of the people in a given environment. Air pollutants are capable of causing respiratory illness, lung disease, asthma, and cancer. Furthermore, unclean environments can spread airborne diseases including common cold and measles due to the presence of pathogens in the air. Environmental testing assists in preventing airborne diseases, thereby ensuring safe living. It helps protect the environment and reduce global warming. Prewel Labs understands the need for environmental testing and helps you achieve a high-quality environment through its testing services. </p>
                </div>
                <!-- /col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->

         <!--  whitebox -->        
         <div class="whitebox py-sm-3">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-5">
                    <!-- col -->
                    <div class="col-lg-12">
                        <h3 class="aos-item pb-2" data-aos="fade-down">At Prewel Labs, We are providing a variety of environmental testing options for You to choose from:</h3>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 col-md-6 aos-item" data-aos="fade-up"> 
                           
                        <h4 class="pt-3 pt-sm-1">Ambient / Work Zone Air Quality Testing</h4>
                        <p>Measure different parameters to estimate the air quality of indoor environments:</p>

                        <ul class="list-items-noicon bglist">
                            <li>NOx, SOx, CO, PM10, PM2.5</li>
                            <li>Ozone (O<sub>3</sub>)</li>
                            <li>Hydrocarbons (HC)</li>
                            <li>Ammonia (NH<sub>3</sub>)</li>
                            <li>Benzene</li>
                            <li>Benzo (a) pyrene</li>
                            <li>Noise, Lux and Ventilation measurements</li>
                            <li>Hydrogen Sulphide (H<sub>2</sub>S)</li>
                            <li>Oxygen</li>
                            <li>Carbon dioxide and Formaldehyde</li>
                            <li>Volatile Organic Carbons (VOCs)</li>
                            <li>Temperature, and Relative Humidity</li>
                        </ul>
                            
                    </div>
                    <!-- /col -->


                    <!-- col -->
                    <div class="col-lg-6 col-md-6 aos-item" data-aos="fade-down">
                           
                        <h4 class="pt-3 pt-sm-1">Stationary Source Emission Testing</h4>
                        <p>Monitor air pollutants emitted by stationary sources such as industries, power plants, refineries, and boilers:</p>                            
                      
                        <ul class="list-items-noicon bglist">
                            <li>Flue gas temperature, velocity, and quantity</li>
                            <li>NOx, SOx, carbon monoxide (CO), and Particulate Matter (PM)</li>
                            <li>Hydrocarbons (HCs)</li>
                            <li>Volatile Organic Carbons (VOCs)</li>
                            <li>Methane</li>
                            <li>Hydrogen Sulphide (H<sub>2</sub>S)</li>
                            <li>Ammonia (NH<sub>3</sub>)</li>
                            <li>Dioxins & Furans</li>
                            <li>Acid Mist</li>
                            <li>Carbon dioxide and Formaldehyde</li>
                            <li>Volatile Organic Carbons (VOCs)</li>
                            <li>Temperature, and Relative Humidity</li>
                        </ul>
                    </div>
                    <!-- /col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
         </div>
         <!--/ white box -->

          <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row py-5">
                <!-- col -->
                <div class="col-lg-6 col-md-6 aos-item" data-aos="fade-up"> 
                    <h4 class="pt-3 pt-sm-1">Breathing, and Process Air Testing</h4>
                    <p>Assess air pollutants present in compressed air, the air we breathe, and process air:</p>
                    <ul class="list-items-noicon bglist">
                        <li>Dew Point</li>
                        <li>Oil content</li>
                        <li>Particulate Matter (PM)</li>
                        <li>Moisture content</li>
                        <li>Viable count for estimating microbial contaminants</li>
                        <li>Oxygen, Carbon dioxide </li>    
                        <li>Carbon monoxide, Nitrogen</li>                            
                        <li>Nitric oxide</li>
                        <li>Sulphur dioxide</li>
                        <li>Volatile Organic Carbons (VOCs)</li>
                        <li>Chlorofluorocarbons (CFCs)</li>
                        <li>Perfluorocarbons (PFCs)</li>
                    </ul>
                </div>
                <!-- /col -->
                <!-- col -->
                <div class="col-lg-6 col-md-6 aos-item" data-aos="fade-down"> 
                    <h4 class="pt-3 pt-sm-1">Industrial Gas Purity Testing</h4>
                    <p>Prewel Labs can also assist you to evaluate the purity of many gases that have industrial applications:</p>                     
                       
                    <ul class="list-items-noicon bglist">
                        <li>Oxygen</li>
                        <li>Hydrogen</li>
                        <li>Nitrogen</li>
                        <li>Helium</li>
                        <li>Natural gas</li>
                        <li>Ammonia</li>
                        <li>Acetylene</li>
                        <li>Ethylene</li>
                        <li>Methanol</li>
                        <li>Neon</li>
                        <li>Carbon dioxide</li>
                        <li>Argon </li>                       
                    </ul>                       
                    
                </div>
                <!-- /col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->

         <!-- section -->
         <div class="whitebox py-2 py-md-5">
            <!-- container -->
            <div class="container">
                <h3 class="text-center">The Sectors that we serve </h3>

                <!-- row -->
                <div class="row justify-content-center pt-2 pt-sm-4">
                    <!-- col -->
                    <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-factory1 icomoon"></span>
                        </div>  
                        <p>Manufacturing Industries  </p>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-Outline icomoon"></span>
                        </div>  
                        <p>IT and MNCs  </p>
                    </div>
                    <!--/ col -->
                    
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-patient icomoon"></span>
                        </div>  
                        <p> Hospitals </p>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-globe icomoon"></span>
                        </div>  
                        <p> Environmental Consultants </p>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-bank icomoon"></span>
                        </div>  
                        <p> Government Authorities </p>
                    </div>
                    <!--/ col -->   

                </div>
                <!--/ row -->
                <!-- row -->                 
            </div>
            <!--/ container -->
        </div>
        <!--/ sectioin -->

    </div>
    <!-- sub page body -->    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
