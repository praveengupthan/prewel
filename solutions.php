<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs</title>  
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Solution</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page">Solution</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body-->
    <div class="subpage-body"> 
        <!-- section -->
        <div class="whitebox py-2 py-md-5">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-8 text-center">
                        <h3>Every problem needs a solution</h3>                        
                        <p class="text-center">Our solutions are tailor made for you as every problem is unique. Our solutions vary depending on the challenges you face. Few of the solutions Prewel Labs is famously known for:</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
                
                <!-- row -->
                <div class="row justify-content-center pt-2 pt-sm-4">
                    <!-- col -->
                    <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-air-conditioner icomoon"></span>
                        </div>  
                        <p> Indoor air quality management & disinfection </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-kitchen icomoon"></span>
                        </div>  
                        <p> Designing of hygiene compliant kitchens </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-production icomoon"></span>
                        </div>  
                        <p> Food product development </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-covid icomoon"></span>
                        </div>  
                        <p> Kitchen sanitization programme</p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-food icomoon"></span>
                        </div>  
                        <p> Food safety solutions for Restaurants </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-catering icomoon"></span>
                        </div>  
                        <p> Food quality ad hygiene training </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-ct-scan icomoon"></span>
                        </div>  
                        <p> Product & Method validation for Medical devices </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-ct-scan-11 icomoon"></span>
                        </div>  
                        <p> Customized study design for Medical devices</p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-ph icomoon"></span>
                        </div>  
                        <p> Water quality management for schools </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-water-pollution icomoon"></span>
                        </div>  
                        <p> Sewage Treatment Plant design and installation </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-laptop icomoon"></span>
                        </div>  
                        <p> E2E solutions for the IT companies </p>
                    </div>
                    <!--/ col -->                    
                </div>
                <!--/ row -->
                         
            </div>
            <!--/ container -->
        </div>
        <!--/ sectioin --> 
      
    </div>
    <!-- sub page body -->
    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
