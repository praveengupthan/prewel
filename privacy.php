<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs Privacy Policy</title>  
    <meta name="description" content="For more information about our terms of use, if you have questions, or if you would like to make a complaint, please contact us by e-mail at support@incepbio.com">
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Privacy</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page">Privacy</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->
    <!-- sub page body-->
    <div class="subpage-body py-4">

    <!-- container -->
    <div class="container">

    <h3>Privacy Policy of Prewellabs</h3>    

    <p>If you require any more information or have any questions about our privacy policy, please feel free to contact us by email at <a href="mailto:support@prewellabs.com">support@prewellabs.com</a></p>

    <p>At <a href="http://prewellabs.com/" target="_blank">www.prewellabs.com</a> we consider the privacy of our visitors to be extremely important. This privacy policy document describes in detail the types of personal information is collected and recorded by www.incepbio.com and how we use it.</p>

    <h4 class="subtitle h4 fgreen"> Log Files</h4> 

    <p>Like many other Web sites, www.incepbio.com makes use of log files. These files merely log visitors to the site - usually a standard procedure for hosting companies and a part of hosting servicer’s analytics. The information inside the log files includes internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date/time stamp, referring/exit pages, and possibly the number of clicks. This information is used to analyze trends, administer the site, track user's movement around the site, and gather demographic information. IP addresses, and other such information are not linked to any information that is personally identifiable.</p>

    <h4 class="subtitle h4 fgreen"> Cookies and Web Beacons</h4> 

    <p><a href="http://prewellabs.com/" target="_blank">www.prewellabs.com </a> uses cookies to store information about visitors' preferences, to record user-specific information on which pages the site visitor accesses or visits, and to personalize or customize our web page content based upon visitors' browser type or other information that the visitor sends via their browser. For more on cookies, visit <a rel="nofollow" href="http://www.allaboutcookies.org">www.allaboutcookies.org</a></p>

    <h4> DoubleClick DART Cookie</h4> 
    <p>→ Google, as a third-party vendor, uses cookies to serve ads on www.incepbio.com. → Google's use of the DART cookie enables it to serve ads to our site's visitors based upon their visit to www.incepbio.com and other sites on the Internet. → Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy at the following URL - <a href="http://www.google.com/privacy_ads.html">http://www.google.com/privacy_ads.html</a></p>

    <h4> Third Party Privacy Policies</h4> 
    <p>You should consult the respective privacy policies of these third-party ad servers for more detailed information on their practices as well as for instructions about how to opt-out of certain practices. www.incepbio.com's privacy policy does not apply to, and we cannot control the activities of, such other advertisers or web sites.</p>

    <p>If you wish to disable cookies, you may do so through your individual browser options. More detailed information about cookie management with specific web browsers can be found at the browsers' respective websites. What Are Cookies?</p>

    <h4 style="color:red;"> Email Opt Out</h4> 
    <p style="color:red;">You can opt out of receiving our marketing emails. To stop receiving our promotional emails, please email <a href="mailto:unsubscribe@prewellabs.com">unsubscribe@prewellabs.com </a> It may take about ten business days to process your request.</p>

    <h4> Child Policy Information</h4> 
    <p>We believe it is important to provide added protection for children online. We encourage parents and guardians to spend time online with their children to observe, participate in and/or monitor and guide their online activity. www.incepbio.com does not knowingly collect any personally identifiable information from children under the age of 16. If a parent or guardian believes that www.incepbio.com has in its database the personally-identifiable information of a child under the age of 16, please contact us immediately (using the contact details) and we will use our best efforts to promptly remove such information from our records.</p>

    <h4> Online Privacy Policy Only</h4> 
    <p>This privacy policy applies only to our online activities and is valid for visitors to our website and regarding information shared and/or collected there. This policy does not apply to any information collected offline or via channels other than this website.</p>

    <h4> Consent</h4> 
    <p>By using our website, you hereby consent to our privacy policy and agree to its terms. We may share information if we think we must in order to comply with the law or to protect ourselves.</p>

    <ul class="list-items">
        <li>We will share information to respond to a court order or subpoena. We may also share it if a government agency or investigatory body requests. Or, we might also share information when we are investigating potential fraud.</li>
        <li>We may use information to protect our company, our customers, or our websites.</li>
        <li>We might send you information about special promotions or offers. We might also tell you about new features or products</li>
        <li>We might use your information to confirm your registration for an Prewellabs event or contest</li>
    </ul>

    <h4> Jurisdiction</h4> 
    <p>If you choose to visit our website, your visit and any dispute over privacy is subject to this Policy and the website's terms of use. In addition to the foregoing, any disputes arising under this Policy shall be governed by the laws of India.</p>

    <h4> Contact Us</h4> 
    <p>For more information about our terms of use, if you have questions, or if you would like to make a complaint, please contact us by e-mail at <a href="mailto:support@prewellabs.com">support@prewellabs.com </a>or by mail using the details provided below:</p>

    <h4> IncepBio Pvt.Ltd.</h4> 
    <p>No. 24, 22nd Main, Marenahalli, J P Nagar 2nd Phase, Bengaluru, karnataka</p>

    <h4> <i>Update</i></h4> 
    <p><i>This Privacy Policy was last updated on:  July 22, 2020.</i></p>
    <p><i>Should we update, amend or make any changes to our privacy policy, those changes will be posted here</i></p>
        

       
    </div>
    <!--/ container -->
    
    </div>
    <!-- sub page body -->    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
