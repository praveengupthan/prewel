<!-- scripts -->
<script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bsnav.min.js"></script>
    <script src="js/popper.js"></script>   
    <script src="js/swiper.min.js"></script>
    <script src="js/custom.js"></script>
     <!-- animation -->
     <script src="js/aos.js"></script>


    <!-- loading smooth-->
    <script src="js/fakeloader.min.js"></script>
    
    <!--[if lt IE 9]>
    <script src="js/html5-shiv.js"></script>
    <![end if ]-->   
    <!-- swiper -->
    <script src="js/swiper.min.js"></script>
    <!-- jquery validation js-->
    <script src='https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js'></script>

    
 

    <script>        
    // aos animation
    AOS.init({
    easing: 'ease-in-out-sine'
    });
    </script>
     <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
    <script>
      $('#datepicker').datepicker({
      uiLibrary: 'bootstrap4'
      });
    </script>    


    <script>
      $(document).ready(function () {
  //enqury form validations
  $('#homeformEnquiry').validate({
      ignore: [],
      errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
      errorElement: 'div',
      errorPlacement: function (error, e) {
          e.parents('.form-group').append(error);
      },
      highlight: function (e) {
          $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
          $(e).closest('.text-danger').remove();
      },
    
      rules: {
          orgName: {
              required: true                
          },
          phNumber: {
              required: true,    
              number: true,
              minlength: 10,
              maxlength:10             
          },  
          mailId:{
            required:true,
            email: true
          },
          pickUpAddress:{
            required:true,
          },
          selService:{
            required:true,
          },
          dtofPickup:{
            required:true,
          },
          timeSchedule:{
            required:true,
          }
          
      },

      messages: {
          orgName: {
              required: "Enter Name"
          },
          phNumber: {
              required: "Enter Valid Mobile Number"                  
          }, 
          mailId:{
            required: "Enter Valid Email"        
          },
          pickUpAddress:{
            required: "Enter Sample Pickup Address"        
          },
          selService:{
            required: "Select Service"        
          },
          dtofPickup:{
            required: "Select Date of Pickup"        
          },
          timeSchedule:{
            required: "Select Time of Pickup"        
          }           
      },
  });



  //contact form validatin
  $('#contact_form').validate({
    ignore: [],
    errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
    errorElement: 'div',
    errorPlacement: function (error, e) {
        e.parents('.form-group').append(error);
    },
    highlight: function (e) {
        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
        $(e).closest('.text-danger').remove();
    },
  
    rules: {
        name: {
            required: true                
        },
        phone: {
            required: true,    
            number: true,
            minlength: 10,
            maxlength:10                   
        },  
        email:{
          required:true,
          email: true
        },
        sub:{
          required:true,
        }  
    },

    messages: {
        name: {
            required: "Enter Name"
        },
        phone: {
            required: "Enter Valid Mobile Number"                  
        }, 
        email:{
          required: "Enter Valid Email"        
        },
        sub:{
          required: "Enter Subject"        
        }            
    },
});

})

</script>



    






