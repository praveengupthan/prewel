<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs Blogs</title>  
    <meta name="description" content="Read our latest blogs on Testing Consultation and solutions">
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Blog</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page">Blog</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body-->
    <div class="subpage-body">       
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row py-4">
                 <!-- col -->
                 <div class="col-lg-4 col-sm-6 aos-item" data-aos="fade-up">
                    <div class="card blogcard">
                        <a href="antiviral-fabric-future-india.php">
                            <img class="card-img-top img-fluid" src="img/blog/antiviral-fabric.jpg" alt="">
                        </a>
                        <div class="card-body position-relative">                           
                            <h6>Antiviral Fabric - Does it have a Future in India?</h6>
                            <p>
                                <small class="fgray">Posted on July 21 2020</small>
                            </p>
                            <a class="d-inline-block round-link" href="antiviral-fabric-future-india.php"><span class="icon-chevron-right icomoon"></span></a>                           
                        </div>
                    </div>
                </div>
                <!--/ col -->
                
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->      
    </div>
    <!-- sub page body -->
    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
