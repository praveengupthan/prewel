<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prewel Labs</title>  
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Medical Device Testing</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Testing </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Medical Device Testing</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body-->
    <div class="subpage-body">
        <!--  whitebox -->        
        <div class="py-5">
            <!-- container-->
            <div class="container">
                <!-- row -->
                <div class="row">
                            
                    <!-- col -->
                    <div class="col-lg-6 col-md-6 align-self-center">
                        <img src="img/medicaldevicetestingimg-01.jpg" alt="" class="img-fluid">
                    </div>
                    <!--col-->       
                    <!-- col -->
                    <div class="col-lg-6 col-md-6 align-self-center">
                    <h2>Why Medical device testing?</h2>
                    <p>Medical device industry is driven by new technologies with continual innovation. Compliance with international standards for medical devices can be complex, and hence a trusted testing partner is the key. Our scientists have extensive backgrounds in handling and testing of single use and reusable medical devices.</p>
                    <p><span class="fblue fbold">Prewel Labs </span> is NABL accredited for ISO 17025:2017 and follows Good laboratory Practices (GLP). We work with your company by adhering to the Quality Management System (QMS) and fulfil your unique needs in medical device testing. </p>
                    <p><span class="fblue fbold">Prewel Labs </span>offers a variety of tests for medical devices.  Most studies are tailored to particular devices.  The lab is an excellent choice for companies that wish to evaluate the performance of antimicrobial devices such as catheters, ports, and wound dressings.  The methods listed below are just a sampling of our capabilities:</p>
                    </div>
                    <!--col-->                      
                </div>
                <!--/ row -->
            </div>
            <!-- container -->               
        </div>
        <!-- / container -->      

        <!-- section -->
        <div class="whitebox py-5">
            <!-- container -->
            <div class="container">
            <h3 class="text-center py-3">Types of Medical Device Testing</h3>
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 aos-item" data-aos="fade-up">
                    <ul class="list-items-noicon bglist">
                        <li>Bioburden Testing</li>
                        <li>Microbial Ingress Test</li>
                        <li>Bacterial Endotoxin Testing Sterility Testing</li>
                        <li>Custom Medical Device Studios</li>                                   
                        <li>Medical Devices Cleaning Validation</li>
                        <li>Bacterial Endotoxins Method Validations</li>
                        <li>Chemical Indicators Evaluation</li>                       
                    </ul>
                </div>
                <!--/col -->
                 <!-- col -->
                 <div class="col-lg-6 aos-item" data-aos="fade-down">
                    <ul class="list-items-noicon bglist">  
                      <li>Antimicrobial Efficacy Testing for Wound Dressings</li>
                      <li>Medical Device Process Validation</li>
                      <li>Sterility Method Validations</li>
                      <li>Cleanroom Microbiological Environmental Monitoring and Qualifications Programs </li>
                      <li>Bioburden Method Validations</li>
                      <li>Biological Indicators Evaluation and Enumeration</li>                      
                    </ul>
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ sectioin -->

    <!-- section -->
    <div class="py-2 py-md-5">
        <!-- container -->
        <div class="container">
            <h3 class="text-center">Why Partner with Prewel Labs?</h3>
            <p class="text-center">With 30 plus years of combined experience in Pharmaceutical industry nobody understands the Pharma regulations as we do. Not just that, our well trained scientists ensure we meet your project needs and exceed your expectations.  Here are some of the benefits of partnering with us:</p>

            <!-- row -->
            <div class="row justify-content-center pt-2 pt-sm-4">
                <!-- col -->
                <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                    <div class="icon">
                        <span class="icon-medal icomoon"></span>
                    </div>  
                    <p> NABL Accredited </p>
                </div>
                <!--/ col -->
                    <!-- col -->
                    <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                    <div class="icon">
                        <span class="icon-factory1 icomoon"></span>
                    </div>  
                    <p> Know how of Medical device industry </p>
                </div>
                <!--/ col -->
                    <!-- col -->
                    <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                    <div class="icon">
                        <span class="icon-gavel icomoon"></span>
                    </div>  
                    <p> Adherence to National & International Regulations </p>
                </div>
                <!--/ col -->                  
                <!-- col -->
                <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                    <div class="icon">
                        <span class="icon-men-1 icomoon"></span>
                    </div>  
                    <p> Strong technical team </p>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                    <div class="icon">
                        <span class="icon-cogs icomoon"></span>
                    </div>  
                    <p> Customized project design </p>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                    <div class="icon">
                        <span class="icon-XMLID_806 icomoon"></span>
                    </div>  
                    <p> On time delivery </p>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                    <div class="icon">
                        <span class="icon-discount icomoon"></span>
                    </div>  
                    <p>  Afforable </p>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
            <!-- row -->                 
        </div>
        <!--/ container -->
    </div>
    <!--/ sectioin -->

  
    </div>
    <!-- sub page body -->    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
