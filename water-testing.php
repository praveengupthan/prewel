<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Water Testing</title>  
    <meta name="description" content="Worried about your water quality call us at 08026589777 to get free consultation about how to get your water quality checked. Our Lab is NABL certified for Water testing">
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Water Testing</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Testing </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Water Testing</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body-->
    <div class="subpage-body">
            <!--  whitebox -->        
            <div class="whitebox py-sm-3">
                <!-- container-->
                <div class="container">
                    <!-- row -->
                    <div class="row py-3">
                    <!-- col -->
                    <div class="col-lg-6 col-md-6 align-self-center aos-item" data-aos="fade-down">
                        <img src="img/deaths.jpg" alt="" class="img-fluid">
                    </div>
                    <!--col-->                    
                    <!--col-->
                     <!-- col -->
                     <div class="col-lg-6 col-md-6 align-self-center aos-item" data-aos="fade-up">
                       <h2>Why test your Water?</h2>
                       <p>Shocking as it may be, it is found that 72% of residents in Bangalore have access to contaminated water alone.</p>
                       <p>According to Eureka Forbes and GFK studies, it has been established that potable water in Bangalore and its surrounding catchment areas plays a central role in causing five major life threatening diseases – acute diarrhea, typhoid, viral hepatitis, cholera and acute encephalitis</p>
                    </div>
                    <!--col-->
                </div>
                <!--/ row -->
                </div>
                <!-- container -->
               
            </div>
        <!-- / container -->

        <!-- sectioin -->
        <div class="py-2 py-3 py-md-5">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-down">
                        <h3>Water Testing lab in Bengaluru</h3>
                        <p>Prewel Labs offers qualitative and quantitative testing services that follows ISO 17025:2017, Bureau of Indian Standards, APHA and FSSAI standards. We empower and assist our customers in implementing comprehensive high quality & safety measures by assuring swift and affordable testing services.</p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-6">
                        <h3 class="text-center py-sm-3">We Serve</h3>

                        <!-- row -->
                        <div class="row justify-content-center">
                            <!-- col -->
                            <div class="col-6 col-sm-4 col-md-3 col-lg-4 icon-div aos-item" data-aos="fade-up">                               
                                <div class="icon">
                                    <span class="icon-living icomoon"></span>
                                </div>  
                                <p>Household and Societies</p>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-6 col-sm-4 col-md-3 col-lg-4 icon-div aos-item" data-aos="fade-down">                               
                                <div class="icon">
                                    <span class="icon-factory icomoon"></span>
                                </div>  
                                <p>Companies and Industries</p>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                             <div class="col-6 col-sm-4 col-md-3 col-lg-4 icon-div aos-item" ata-aos="fade-up">                               
                                <div class="icon">
                                    <span class="icon-patient icomoon"></span>
                                </div>  
                                <p>Hospitals</p>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                             <div class="col-6 col-sm-4 col-md-3 col-lg-4 icon-div aos-item" ata-aos="fade-down">                               
                                <div class="icon">
                                    <span class="icon-chef icomoon"></span>
                                </div>  
                                <p>Restaurants & Eateries</p>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-6 col-sm-4 col-md-3 col-lg-4 icon-div aos-item" data-aos="fade-up">                               
                                <div class="icon">
                                    <span class="icon-school icomoon"></span>
                                </div>  
                                <p>Schools & Colleges</p>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!-- sectioin -->

        <!-- section -->
        <div class="whitebox py-2 py-md-5">
            <!-- container -->
            <div class="container">
                <h3 class="text-center">Various Water testing options Prewel Labs provides</h3>

                <!-- row -->
                <div class="row justify-content-center pt-2 pt-sm-4">
                    <!-- col -->
                    <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-water icomoon"></span>
                        </div>  
                        <p> Bore Well Water </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-wheel-barrow icomoon"></span>
                        </div>  
                        <p> Construction Water </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-water-well icomoon"></span>
                        </div>  
                        <p> Well Water </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-hand-wash icomoon"></span>
                        </div>  
                        <p> Tap Water </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-tank icomoon"></span>
                        </div>  
                        <p> STP Water </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-pool icomoon"></span>
                        </div>  
                        <p> Swimming pool water </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-md-3 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-bridge icomoon"></span>
                        </div>  
                        <p> ETP water </p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
                <!-- row -->                 
            </div>
            <!--/ container -->
        </div>
        <!--/ sectioin -->

        <!-- blue sub section -->
        <div class="benefits-section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-6 text-center">
                        <h4>Benefits of Water Testing</h4>                        
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
                <!-- row -->
                <div class="row justify-content-center py-2 my-sm-3">
                    <!--col-->
                    <div class="col-6 col-sm-3 col-lg-2 benefits-col text-center aos-item" data-aos="fade-up">
                        <span class="icon-heartbeat icomoon"></span>
                        <p>Protection of your health and in nurturing your well being</p>
                    </div>
                    <!--/ col -->
                     <!--col-->
                     <div class="col-6 col-sm-3 col-lg-2 benefits-col text-center aos-item" data-aos="fade-down">
                        <span class="icon-sick icomoon"></span>
                        <p>Protection from Water borne diseases</p>
                    </div>
                    <!--/ col -->
                     <!--col-->
                     <div class="col-6 col-sm-3 col-lg-2 benefits-col text-center aos-item" data-aos="fade-up">
                        <span class="icon-skin icomoon"></span>
                        <p>Protection from skin infections</p>
                    </div>
                    <!--/ col -->
                      <!--col-->
                      <div class="col-6 col-sm-3 col-lg-2 benefits-col text-center aos-item" data-aos="fade-down">
                        <span class="icon-scalp icomoon"></span>
                        <p>Avoid Hair fall</p>
                    </div>
                    <!--/ col -->
                     <!--col-->
                     <div class="col-6 col-sm-3 col-lg-2 benefits-col text-center aos-item" data-aos="fade-up">
                        <span class="icon-kitchen icomoon"></span>
                        <p>Avoid damage to your home appliances</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ blue sub section -->

        <!-- container -->
        <div class="container whitebox highlets">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-4 col-sm-6  border-right highletscol aos-item" data-aos="fade-up">
                    <div class="d-flex justify-content-center">
                        <span class="icon-research icomoon"></span>
                        <div class="align-self-center">
                            <h5 class="fbold">2500+</h5>
                            <p class="fgray">Drinking water samples</p>
                        </div>
                    </div>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-4 col-sm-6 border-right highletscol aos-item" data-aos="fade-down">
                    <div class="d-flex justify-content-center">
                        <span class="icon-tank icomoon"></span>
                        <div class="align-self-center">
                            <h5 class="fbold">1500+</h5>
                            <p class="fgray">STP water samples</p>
                        </div>
                    </div>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-4 col-sm-6 highletscol aos-item" data-aos="fade-up">
                    <div class="d-flex justify-content-center">
                        <span class="icon-hot-pool icomoon"></span>
                        <div class="align-self-center">
                            <h5 class="fbold">500+</h5>
                            <p class="fgray">Swimming pool samples</p>
                        </div>
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!-- sub page body -->
    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
