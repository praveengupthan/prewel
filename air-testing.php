<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Indoor Air Testing</title>  
    <meta name="description" content="Do you think your Indoor Air quality has gone down? You can call us at 08026589777 to get a free consultation. We are NABL certified for Air Testing.">
    <?php include 'styles.php'?>

</head>
<body>
   
    <div id="fakeloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>  
    <?php include 'header.php'?>

    <!--main-->
    <main class="subPage">

    <!-- subpage header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <article>
                <h1>Indoor Air Testing</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://prewellabs.com/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Testing </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Air Testing</li>
                    </ol>
                </nav>
            </article>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->

    <!-- sub page body-->
    <div class="subpage-body">
        <!--  whitebox -->        
            <div class="py-sm-5">
                <!-- container-->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                            
                    <!--col-->
                     <!-- col -->
                     <div class="col-lg-6 align-self-center aos-item" data-aos="fade-up">
                       <h2>Why Indoor Air Testing ?</h2>
                       <p>The quality of air in indoor environments, especially in office spaces and other building spaces, tend to be more polluted than the air outside owing to their closed confinement. Chemical and gas pollutants can get accumulated over period of time leading to health risks ranging from headaches, eye-irritation and allergies to serious issues involving asthma, lung cancer and heart problems. With one spending a significant amount of their time indoors, it is absolutely quintessential for indoor air to be of top notch quality for one’s good health and overall well-being. Air testing can assure you of your indoor air quality and lay your worries to rest.</p>
                    </div>
                    <!--col-->
                     <!-- col -->
                     <div class="col-lg-6 align-self-center aos-item" data-aos="fade-down">
                     <h3 class="text-center py-sm-3">We Serve</h3>

                      <!-- row -->
                      <div class="row justify-content-center">
                           <!-- col -->
                           <div class="col-6 col-sm-4 icon-div aos-item" data-aos="fade-up">                               
                                <div class="icon">
                                    <span class="icon-factory icomoon"></span>
                                </div>  
                                <p>Companies and Industries</p>
                            </div>
                            <!--/ col -->
                            
                             <!-- col -->
                             <div class="col-6 col-sm-4 icon-div aos-item" data-aos="fade-down">                               
                                <div class="icon">
                                    <span class="icon-patient icomoon"></span>
                                </div>  
                                <p>Hospitals</p>
                            </div>
                            <!--/ col --> 

                             <!-- col -->
                             <div class="col-6 col-sm-4 icon-div aos-item" data-aos="fade-up">                               
                                <div class="icon">
                                    <span class="icon-chef icomoon"></span>
                                </div>  
                                <p>Restaurants & Eateries</p>
                            </div>
                            <!--/ col -->                  
                        </div>
                        <!--/ row -->                        
                    </div>
                    <!--col-->           
                </div>
                <!--/ row -->
                </div>
                <!-- container -->               
            </div>
            <!-- / container -->      

        <!-- section -->
        <div class="whitebox py-2 py-md-5">
            <!-- container -->
            <div class="container">
            <h3 class="text-center">Various Air Testing Options That We Provide</h3>

                <!-- row -->
                <div class="row justify-content-center pt-2 pt-sm-4">
                    <!-- col -->
                    <div class="col-6 col-sm-4 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-Gas-Canister icomoon"></span>
                        </div>  
                        <p> Gas testing CO2, CO, SOX and NOX </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-lg-2 icon-div aos-item" data-aos="fade-up">                               
                        <div class="icon">
                            <span class="icon-thermometer icomoon"></span>
                        </div>  
                        <p> Temperature and relative humidity </p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-6 col-sm-4 col-lg-2 icon-div aos-item" data-aos="fade-down">                               
                        <div class="icon">
                            <span class="icon-soundcloud icomoon"></span>
                        </div>  
                        <p> Noise level </p>
                    </div>
                    <!--/ col -->                              
                </div>
                <!--/ row -->
                <!-- row -->                 
            </div>
            <!--/ container -->
        </div>
        <!--/ sectioin -->

        <!-- blue sub section -->
        <div class="benefits-section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-6 text-center aos-item" data-aos="fade-down">
                        <h4>Benefits of air testing</h4>                        
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
                <!-- row -->
                <div class="row justify-content-center py-2 my-sm-3">
                    <!--col-->
                    <div class="col-6 col-sm-3 benefits-col text-center aos-item" data-aos="fade-up">
                        <span class="icon-skin icomoon"></span>                       
                        <p> Dry and itchy skin </p>
                    </div>
                    <!--/ col -->
                     <!--col-->
                     <div class="col-6 col-sm-3 benefits-col text-center aos-item" data-aos="fade-down">
                        <span class="icon-sore-throat icomoon"></span>                       
                        <p>Eye or throat irritation</p>
                    </div>
                    <!--/ col -->
                     <!--col-->
                     <div class="col-6 col-sm-3 benefits-col text-center aos-item" data-aos="fade-up">
                        <span class="icon-dizzy icomoon"></span>                     
                        <p>Dizziness</p>
                    </div>
                    <!--/ col -->
                      <!--col-->
                      <div class="col-6 col-sm-3 benefits-col text-center aos-item" data-aos="fade-down">
                        <span class="icon-burnout icomoon"></span>                       
                        <p>Fatigue</p>
                    </div>
                    <!--/ col -->
                    
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ blue sub section -->

        <!-- container -->
        <div class="container whitebox highlets">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-lg-4 highletscol aos-item" data-aos="fade-up">
                    <div class="d-flex justify-content-center">
                        <span class="icon-research icomoon"></span>
                        <div class="align-self-center">
                            <h5 class="fbold">500+</h5>
                            <p class="fgray">Indoor Air samples tested</p>
                        </div>
                    </div>
                </div>
                <!--/ col -->                
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!-- sub page body -->
   

   

    
   
    

    
    </main>
    <!--/ main ends -->

    <?php include 'footer.php'?>
    <?php include 'scripts.php' ?>
</body>
</html>
